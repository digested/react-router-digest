<div align="center">
  <h1>
    <a
      href="https://gitlab.com/digested/dgui/"
    >
      @DGUI
    </a>
  </h1>
</div>

<div align="center">
  <a
    href="https://commitizen.github.io/cz-cli/"
  >
    <img
      alt="Commitizen Friendly"
      src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000"
    />
  </a>
  <a
    href="https://github.com/semantic-release/semantic-release"
  >
    <img
      alt="Semantic Release"
      src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000"
    />
  </a>
</div>

<div align="center">
  <a
    href="https://gitlab.com/digested/dgui/-/commits/main"
  >
    <img
      alt="Build Status"
      src="https://gitlab.com/digested/dgui/badges/main/pipeline.svg"
    />
  </a>
  <a
    href="https://digested.gitlab.io/dgui/coverage"
  >
    <img
      alt="Coverage Report"
      src="https://gitlab.com/digested/dgui/badges/master/coverage.svg"
    />
  </a>
  <a
    href="https://www.npmjs.org/package/@dgui/react-shared"
  >
    <img
      alt="NPM Version"
      src="http://img.shields.io/npm/v/@dgui/react-shared.svg?maxAge=86400"
    />
  </a>
  <a
    href="https://www.gnu.org/licenses/lgpl-3.0.html"
  >
    <img
      alt="License"
      src="https://img.shields.io/npm/l/@dgui/react-shared.svg?maxAge=2592000"
    />
  </a>
  <a
    href="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fdgui.svg?type=shield"
  >
    <img
      alt="FOSSA Status"
      src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fdgui.svg?type=shield&maxAge=3600"
    />
  </a>
  <a
    href="https://github.com/gajus/canonical"
  >
    <img
      alt="Canonical Code Style"
      src="https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000"
    />
  </a>
</div>
<br />
<br />

## About

**`@dgui`** is a set of composable components to support tab and drawer interfaces.

### Cross Platform

Component varieties include `react`, `react-native`, and `react-native-web` *and* share the same API. Routing is supported with [`react-router`](https://reactrouter.com/en/main). Other routers could be supported in the future. The initial interface is purposefully simple. For material style tabs, see `@dgui/*-material-bars` packages for usage documentation or example code.

For help building a cross platform app with shared components, watch [this](https://www.youtube.com/watch?v=ORsUbTlRep0). See the project [demo source](https://gitlab.com/digested/dgui/-/tree/next/demo) for examples.

## Getting Started

### Basic Example

#### Installing

```bash
npm install @dgui/react-router-web react react-dom react-router react-router-dom react-modern-drawer swiper
```

#### Usage

```ts
export {
  ReactRouterWebDigest as ReactDigest,
  ReactRouterWebDigestAppBar as AppBar,
  ReactRouterWebDigestDrawer as Drawer,
  ReactRouterWebDigestDrawerBar as DrawerBar,
  ReactRouterWebDigestTab as Tab
} from '@dgui/react-router-web';
import 'react-modern-drawer/dist/index.css';
import 'swiper/css';
import React, {
  type FunctionComponent
} from 'react';

const App: FunctionComponent = () => {
  return (
    <Preliminary>
      <ReactDigest
        tabIndex={0}
      >
        <AppBar />
        <DrawerBar />
        <Drawer
          drawerId='dials'
          tabId='tab1'
        >
          <p>
            tab1
          </p>
        </Drawer>
        <Drawer
          drawerId='dials'
          tabId='tab2'
        >
          <p>
            tab2
          </p>
        </Drawer>
        <Tab
          tabId='tab1'
          tabPath='tabs/:tabId'
        >
          <p>
            test
          </p>
        </Tab>
        <Tab
          tabId='tab2'
          tabPath='tabs/:tabId'
        >
          <p>
            test2
          </p>
        </Tab>
        <Drawer
          drawerId='drawer2'
        >
          <p>
            drawer2
          </p>
        </Drawer>
      </ReactRouterDigest>
    </Preliminary>
  );
};

export default App;
```

## API & Packages

* **[@dgui/react-* API](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*)**
  * [@dgui/react-web](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91web)
  * [@dgui/react-native](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91native)
  * [@dgui/react-native-web](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91native%E2%80%91web)
* **[@dgui/react-router-* API](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91*)** (*previous components with extended API*)
  * [@dgui/react-router-web](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91web)
  * [@dgui/react-router-native](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91native)
  * [@dgui/react-router-native-web](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91*/@dgui%E2%A7%B8react%E2%80%91router%E2%80%91native%E2%80%91web)
* **[@dgui⧸react‑*‑material‑bars API](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*%E2%80%91material%E2%80%91bars)**
  * [@dgui/react-web-material-bars](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*%E2%80%91material%E2%80%91bars/@dgui%E2%A7%B8react%E2%80%91web%E2%80%91material%E2%80%91bars)
  * [@dgui/react-native-material-bars](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*%E2%80%91material%E2%80%91bars/@dgui%E2%A7%B8react%E2%80%91native%E2%80%91material%E2%80%91bars)
  * [@dgui/react-native-web-material-bars](https://gitlab.com/digested/dgui/-/wikis/@dgui%E2%A7%B8react%E2%80%91*%E2%80%91material%E2%80%91bars/@dgui%E2%A7%B8react%E2%80%91native%E2%80%91web-material%E2%80%91bars)
