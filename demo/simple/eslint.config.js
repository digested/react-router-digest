/*
  eslint-disable
  unicorn/prefer-module
*/

const config = require('@digest/eslint-config');

module.exports = [
  {
    ignores: [
      'android/*',
      'ios/*'
    ]
  },
  ...config
];
