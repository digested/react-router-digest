import Preliminary from './Components/Preliminary';
import {
  ReactRouterDigestAppBar as AppBar,
  ReactRouterDigestDrawer as Drawer,
  ReactRouterDigestDrawerBar as DrawerBar,
  ReactRouterDigest,
  ReactRouterDigestTab as Tab
} from './Components/ReactRouter';
import {
  Paragraph
} from './Components/tags';
import React, {
  type FunctionComponent
} from 'react';

const App: FunctionComponent = () => {
  return (
    <Preliminary>
      <ReactRouterDigest
        // basename='/proxy/8090/'

        // eslint-disable-next-line react/jsx-props-no-multi-spaces
        tabIndex={0}
      >
        <AppBar />
        <DrawerBar />
        <Drawer
          drawerId='dials'
          tabId='tab1'
        >
          <Paragraph>
            tab1
          </Paragraph>
        </Drawer>
        <Drawer
          drawerId='dials'
          tabId='tab2'
        >
          <Paragraph>
            tab2
          </Paragraph>
        </Drawer>
        <Tab
          tabId='tab1'
          tabPath='tabs/:tabId'
        >
          <Paragraph>
            test
          </Paragraph>
        </Tab>
        <Tab
          tabId='tab2'
          tabPath='tabs/:tabId'
        >
          <Paragraph>
            test2
          </Paragraph>
        </Tab>
        <Drawer
          drawerId='drawer2'
        >
          <Paragraph>
            drawer2
          </Paragraph>
        </Drawer>
      </ReactRouterDigest>
    </Preliminary>
  );
};

export default App;
