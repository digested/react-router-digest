import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Preliminary: FunctionComponent<PreliminaryProps> = ({
  children
}) => {
  return (
    <>
      {children}
    </>
  );
};

export default Preliminary;

export type PreliminaryProps = PropsWithChildren<object>;
