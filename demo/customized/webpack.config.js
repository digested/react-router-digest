/*

Copyright (C) 2016,2017 wallzero 2016,2017

This program is free software: you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software Foundation,
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>.

*/

/*
  eslint-disable
  unicorn/prefer-module,
  canonical/filename-match-exported
*/

// eslint-disable-next-line n/no-extraneous-require, import/no-extraneous-dependencies
const webpackDigest = require('@digest/webpack');
const path = require('node:path');

webpackDigest.module.rules = [
  ...webpackDigest.module.rules
];

webpackDigest.resolve.alias = {
  ...webpackDigest.resolve.alias,
  'color-name': path.join(
    __dirname,
    'node_modules',
    'color-name'
  ),
  'react-is': path.join(
    __dirname,
    'node_modules',
    'react-is'
  )
};

module.exports = webpackDigest;
