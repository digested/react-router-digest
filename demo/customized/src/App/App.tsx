import {
  Paragraph
} from './Components/Generics/tags';
import {
  ReactRouterDigestDrawer as Drawer,
  ReactDigestAppBar,
  ReactDigestDrawerBar,
  ReactRouterDigest,
  ReactRouterDigestTab as Tab
} from './Components/ReactRouter';
import Preliminary from './Preliminary';
import React, {
  type FunctionComponent
} from 'react';

const App: FunctionComponent = () => {
  return (
    <Preliminary>
      <ReactRouterDigest

        // eslint-disable-next-line react/jsx-props-no-multi-spaces
        tabIndex={0}
      >
        <ReactDigestAppBar />
        <ReactDigestDrawerBar />
        <Drawer
          drawerId='tabs'
          tabId='tab1'
          title='Tabs'
        >
          <Paragraph>
            Tab 1
          </Paragraph>
        </Drawer>
        <Drawer
          drawerId='tabs'
          tabId='tab2'
          title='Tabs'
        >
          <Paragraph>
            Tab 2
          </Paragraph>
        </Drawer>
        <Tab
          tabId='tab1'
          tabPath='tabs/:tabId'
          title='Tab 1'
        >
          <Paragraph>
            test
          </Paragraph>
        </Tab>
        <Tab
          tabId='tab2'
          tabPath='tabs/:tabId'
          title='Tab 2'
        >
          <Paragraph>
            Test 2
          </Paragraph>
        </Tab>
        <Drawer
          drawerId='settings'
          title='Settings'
        >
          <Paragraph>
            Settings
          </Paragraph>
        </Drawer>
      </ReactRouterDigest>
    </Preliminary>
  );
};

export default App;
