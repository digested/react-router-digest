import MaterialUIProvider from './MaterialUIProvider';
import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children
}) => {
  return (
    <MaterialUIProvider>
      {children}
    </MaterialUIProvider>
  );
};

Providers.displayName = 'Providers';

export default Providers;

export type ProvidersProps = PropsWithChildren<object>;
