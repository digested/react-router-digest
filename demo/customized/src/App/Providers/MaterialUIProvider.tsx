import CssBaseline from '@mui/material/CssBaseline';
import {
  createTheme,
  ThemeProvider
} from '@mui/material/styles';
import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

// A custom theme for this app
const theme = createTheme();

const MaterialUIProvider: FunctionComponent<MaterialUIProviderProps> = ({
  children
}) => {
  return (
    <ThemeProvider
      theme={theme}
    >
      <CssBaseline>
        {children}
      </CssBaseline>
    </ThemeProvider>
  );
};

MaterialUIProvider.displayName = 'MaterialUIProvider';

export default MaterialUIProvider;

export type MaterialUIProviderProps = PropsWithChildren<object>;
