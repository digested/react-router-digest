// eslint-disable-next-line import/no-unassigned-import
import './vendor';
// eslint-disable-next-line import/no-unassigned-import
import './index.g.css';
import App from './App';
import React, {
  StrictMode
} from 'react';
import {
  createRoot
} from 'react-dom/client';

const rootElement = document.querySelector('#app') as HTMLElement;

const root = createRoot(
  rootElement
);

root.render(
  <StrictMode>
    <App />
  </StrictMode>
);

export {
  default
} from './App';
