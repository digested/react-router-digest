import appJson from '../app.json';
import App from './App';
import {
  AppRegistry,
  Platform
} from 'react-native';

AppRegistry.registerComponent(
  appJson.name,
  () => {
    return App;
  }
);

if (Platform.OS === 'web') {
  AppRegistry.runApplication(
    appJson.name,
    {
      rootTag: document.querySelector('#app')
    }
  );
}

export {
  default
} from './App';
