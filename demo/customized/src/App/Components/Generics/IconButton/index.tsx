import {
  IconButton as GenericIconButton,
  type IconButtonProps as GenericIconButtonProps,
  PLATFORM
} from '../tags';
import React, {
  type FunctionComponent
} from 'react';

const IconButton: FunctionComponent<IconButtonProps> = ({
  accessibilityLabel,
  children,
  color,
  onClick,
  variant,
  ...rest
}) => {
  return (
    <GenericIconButton
      {
        ...(PLATFORM === 'web' ?
          {
            children,
            color,
            onClick
          } :
          {
            accessibilityLabel,
            buttonColor: color,
            icon: 'menu',
            mode: variant,
            onPress: onClick
          }
        )
      }
      {...rest}
    />
  );
};

export default IconButton;

export type IconButtonProps = GenericIconButtonProps & {
  /**
   * Only for native
   */
  readonly accessibilityLabel: string;
  /**
   * Only for native
   */
  readonly variant?: 'contained' | 'contained-tonal' | 'outlined';
};
