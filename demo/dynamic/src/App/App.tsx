import {
  Button,
  Paragraph,
  Text
} from './Components/Generics';
import {
  ReactRouterDigestDrawer as Drawer,
  ReactDigestAppBar,
  ReactDigestDrawerBar,
  ReactRouterDigest,
  ReactRouterDigestTab as Tab
} from './Components/ReactRouter';
import Preliminary from './Preliminary';
import React, {
  type FunctionComponent,
  type ReactElement,
  useCallback,
  useMemo,
  useRef,
  useState
} from 'react';

const App: FunctionComponent = () => {
  const [
    tabs,
    setTabs
  ] = useState([
    {
      path: 'tabs/:tabId',
      tabId: 'tab1',
      title: 'Tab 1'
    },
    {
      path: 'tabs/:tabId',
      tabId: 'tab2',
      title: 'Tab 2'
    }
  ]);

  const tabCount = useRef(tabs.length + 10);

  const addTab = useCallback(
    () => {
      setTabs(
        (
          currentTabs
        ) => {
          tabCount.current++;

          return [
            ...currentTabs,
            {
              path: 'tabs/:tabId',
              tabId: `tab${tabCount.current}`,
              title: `Tab ${tabCount.current}`
            }
          ];
        }
      );
    },
    []
  );

  const removeTab = useCallback(
    () => {
      setTabs(
        (
          currentTabs
        ) => {
          if (currentTabs.length > 1) {
            return currentTabs.slice(
              0,
              -1
            );
          } else {
            return currentTabs;
          }
        }
      );
    },
    []
  );

  const Tabs: Array<ReactElement<typeof Tab>> = useMemo(
    () => {
      return tabs.map(
        ({
          path,
          tabId,
          title
        }) => {
          return (
            <Tab
              key={tabId}
              tabId={tabId}
              tabPath={path}
              title={title}
            >
              <Paragraph>
                {title}
              </Paragraph>
            </Tab>
          );
        }
      );
    },
    [
      tabs
    ]
  );

  const Drawers: Array<ReactElement<typeof Drawer>> = useMemo(
    () => {
      const DrawerTabs = tabs.map(
        ({
          tabId,
          title
        }) => {
          return (
            <Drawer
              drawerId='tabs'
              key={tabId}
              tabId={tabId}
              title='Tabs'
            >
              <Paragraph>
                {title}
              </Paragraph>
            </Drawer>
          );
        }
      );

      DrawerTabs.push(
        <Drawer
          drawerId='settings'
          key='settings'
          title='Settings'
        >
          <Paragraph>
            Settings
          </Paragraph>
          <Button
            onClick={addTab}
            variant='contained'
          >
            <Text>
              Add Tab
            </Text>
          </Button>
          <Button
            disabled={tabs.length < 2}
            onClick={removeTab}
            variant='contained'
          >
            <Text>
              Remove Tab
            </Text>
          </Button>
        </Drawer>
      );

      return DrawerTabs;
    },
    [
      addTab,
      removeTab,
      tabs
    ]
  );

  return (
    <Preliminary>
      <ReactRouterDigest
        drawerIndex={1}
        drawerOpen
        // eslint-disable-next-line jsx-a11y/tabindex-no-positive
        tabIndex={1}
      >
        <ReactDigestAppBar />
        <ReactDigestDrawerBar />
        {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          Drawers as unknown as any
        }
        {
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          Tabs as unknown as any
        }
      </ReactRouterDigest>
    </Preliminary>
  );
};

export default App;
