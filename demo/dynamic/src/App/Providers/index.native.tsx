/*
  eslint-disable
  canonical/filename-match-exported
*/
import ReactNativePaperProvider from './ReactNativePaperProvider';
import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children
}) => {
  return (
    <ReactNativePaperProvider>
      {children}
    </ReactNativePaperProvider>
  );
};

Providers.displayName = 'Providers';

export default Providers;

export type ProvidersProps = PropsWithChildren<object>;
