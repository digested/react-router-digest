import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  Provider as PaperProvider
} from 'react-native-paper';

const ReactNativePaperProvider: FunctionComponent<ReactNativePaperProviderProps> = ({
  children
}) => {
  return (
    <PaperProvider>
      {children}
    </PaperProvider>
  );
};

ReactNativePaperProvider.displayName = 'ReactNativePaperProvider';

export default ReactNativePaperProvider;

export type ReactNativePaperProviderProps = PropsWithChildren<object>;
