import Providers from './Providers';
import React, {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Preliminary: FunctionComponent<PreliminaryProps> = ({
  children
}) => {
  return (
    <Providers>
      {children}
    </Providers>
  );
};

export default Preliminary;

export type PreliminaryProps = PropsWithChildren<object>;
