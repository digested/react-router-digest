export {
  View as Container,
  Text as Paragraph,
  Text
} from 'react-native';
export {
  Button,
  type ButtonProps,
  IconButton,
  type IconButtonProps
} from 'react-native-paper';

const PLATFORM = 'native';

export {
  PLATFORM
};
