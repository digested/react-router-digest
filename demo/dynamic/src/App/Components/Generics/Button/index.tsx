import {
  type ButtonProps,
  Button as GenericButton,
  PLATFORM
} from '../tags';
import React, {
  type FunctionComponent
} from 'react';

const Button: FunctionComponent<ButtonProps> = ({
  children,
  color,
  onClick,
  variant,
  ...rest
}) => {
  return (
    <GenericButton
      {
        ...(PLATFORM === 'web' ?
          {
            color,
            onClick,
            variant
          } :
          {
            buttonColor: color,
            mode: variant,
            onPress: onClick
          }
        )
      }
      {...rest}
    >
      {children}
    </GenericButton>
  );
};

export default Button;

export {
  type ButtonProps
} from '../tags';
