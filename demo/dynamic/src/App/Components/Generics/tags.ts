export {
  default as Button,
  type ButtonProps
} from '@mui/material/Button';
export {
  default as IconButton,
  type IconButtonProps
} from '@mui/material/IconButton';
export const Text = 'span';
export const Paragraph = 'p';
export const Container = 'div';

const PLATFORM = 'web';

export {
  PLATFORM
};
