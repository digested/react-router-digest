import {
  type ReactDigestAppBarProps,
  type ReactDigestDrawerBarProps,
  type ReactDigestDrawerProps,
  type ReactDigestDrawersProps,
  type ReactDigestProps,
  type ReactDigestTabProps
} from '@dgui/react-shared';

export type ReactNativeWebDigestAppBarProps = ReactDigestAppBarProps;

export type ReactNativeWebDigestDrawerBarProps = ReactDigestDrawerBarProps;

export type ReactNativeWebDigestDrawerProps = ReactDigestDrawerProps;

export type ReactNativeWebDigestDrawersProps = ReactDigestDrawersProps;

export type ReactNativeWebDigestProps = ReactDigestProps;

export type ReactNativeWebDigestTabProps = ReactDigestTabProps;

