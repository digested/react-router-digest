import {
  type ReactNativeWebDigestDrawerBarProps
} from '../../Types';
import {
  DrawerBar as SharedDrawerBar
} from '@dgui/react-native-shared';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent
} from 'react';

const NativeWebDrawerBar: FunctionComponent<ReactNativeWebDigestDrawerBarProps> = ({
  children
}) => {
  return (
    <WebBaseGateway
      gatewayId='ReactNativeWebDigestDrawerBar'
    >
      {children ?? <SharedDrawerBar />}
    </WebBaseGateway>
  );
};

NativeWebDrawerBar.displayName = 'NativeWebDrawerBar';

export default NativeWebDrawerBar;
