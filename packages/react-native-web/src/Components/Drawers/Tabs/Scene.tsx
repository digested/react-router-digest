/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prevent-abbreviations
*/

import {
  NativeWebGatewayDest
} from '../../../Generics/NativeWebGateway';
import Context from './Context';
import {
  useDrawers
} from '@dgui/react-shared';
import {
  WebBaseHorse
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = ({
  drawerId
}) => {
  const drawers = useDrawers();

  const {
    swipe,
    tabs
  } = drawers[drawerId];

  const gatewayId = useMemo(
    () => {
      return `drawer-${drawerId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <WebBaseHorse
      swipe={swipe}
    >
      {
        tabs ?
          // eslint-disable-next-line @stylistic/no-extra-parens
          (
            <Context
              drawerId={drawerId}
            />
          ) :
          // eslint-disable-next-line @stylistic/no-extra-parens
          (
            <NativeWebGatewayDest
              gatewayId={gatewayId}
            />
          )
      }
    </WebBaseHorse>
  );
};

Scene.displayName = 'NativeWebDrawersTabsScene';

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly drawerId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
