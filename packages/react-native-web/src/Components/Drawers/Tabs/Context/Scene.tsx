/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prevent-abbreviations
*/

import {
  NativeWebGatewayDest
} from '../../../../Generics/NativeWebGateway';
import {
  useTabs
} from '@dgui/react-shared';
import {
  WebBaseHorse
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = (
  {
    drawerId,
    tabId
  }
) => {
  const tabs = useTabs();

  const {
    swipe
  } = tabs[tabId];

  const gatewayId = useMemo(
    () => {
      return `drawer-${drawerId}-${tabId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <WebBaseHorse
      swipe={swipe}
    >
      <NativeWebGatewayDest
        gatewayId={gatewayId}
      />
    </WebBaseHorse>
  );
};

Scene.displayName = 'NativeWebDrawersTabsSceneContextScene';

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly drawerId: string;
  readonly tabId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
