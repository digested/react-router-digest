import Scene from './Scene';
import {
  useSelectTab,
  useTabIndex,
  useTabOrder
} from '@dgui/react-shared';
import {
  WebBaseCarousel
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Context: FunctionComponent<ContextProps> = ({
  drawerId
}) => {
  const selectTab = useSelectTab();
  const tabIndex = useTabIndex();
  const tabOrder = useTabOrder();

  const tabs = useMemo(
    () => {
      return tabOrder.map(
        (tab) => {
          return (
            <Scene
              drawerId={drawerId}
              key={tab}
              tabId={tab}
            />
          );
        }
      );
    },
    [
      drawerId,
      tabOrder
    ]
  );

  return (
    <WebBaseCarousel
      onSelectTab={selectTab}
      swipe={false}
      tabIndex={tabIndex}
    >
      {tabs}
    </WebBaseCarousel>
  );
};

Context.displayName = 'NativeWebDrawersTabsSceneContext';

export default memo(Context);

export type ContextProps = {
  readonly drawerId: string;
};
