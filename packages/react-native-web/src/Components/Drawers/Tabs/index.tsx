import Scene from './Scene';
import {
  useDrawerIndex,
  useDrawerOrder,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  WebBaseCarousel
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Drawers: FunctionComponent<DrawerProps> = () => {
  const drawerOrder = useDrawerOrder();
  const drawerIndex = useDrawerIndex();
  const selectDrawer = useSelectDrawer();

  const drawers = useMemo(
    () => {
      return drawerOrder.map(
        (drawerId) => {
          return (
            <Scene
              drawerId={drawerId}
              key={drawerId}
            />
          );
        }
      );
    },
    [
      drawerOrder
    ]
  );

  return (
    <WebBaseCarousel
      onSelectTab={selectDrawer}
      tabIndex={drawerIndex}
    >
      {drawers}
    </WebBaseCarousel>
  );
};

Drawers.displayName = 'NativeWebDrawersTabs';

export default memo(Drawers);

export type DrawerProps = object;
