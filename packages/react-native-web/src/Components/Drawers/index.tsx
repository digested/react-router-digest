import {
  NativeWebGatewayDest as GatewayDestination
} from '../../Generics/NativeWebGateway';
import {
  type ReactNativeWebDigestDrawersProps
} from '../../Types';
import Tabs from './Tabs';
import {
  NativeBaseDrawers as SideDrawers
} from '@dgui/react-native-shared';
import {
  useDrawerOpen
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';
import {
  type ViewStyle

} from 'react-native';

const Drawers: FunctionComponent<DrawersProps> = (
  props
) => {
  const drawerOpen = useDrawerOpen();

  const appBar = useMemo(
    () => {
      return (
        <>
          <GatewayDestination
            gatewayId='ReactNativeWebDigestDrawerBar'
          />
          <Tabs />
        </>
      );
    },
    []
  );

  return (
    <SideDrawers
      drawerContent={appBar}
      open={drawerOpen}
      {...props}
    />
  );
};

Drawers.displayName = 'NativeWebDrawers';

export default Drawers;

export type DrawersProps = Omit<PropsWithChildren<ReactNativeWebDigestDrawersProps>, 'style'> & {
  readonly style: ViewStyle;
};

