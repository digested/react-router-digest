import {
  type ReactNativeWebDigestAppBarProps
} from '../../Types';
import {
  AppBar as SharedAppBar
} from '@dgui/react-native-shared';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent
} from 'react';

const NativeWebAppBar: FunctionComponent<ReactNativeWebDigestAppBarProps> = ({
  children
}) => {
  return (
    <WebBaseGateway
      gatewayId='ReactNativeWebDigestAppBar'
    >
      {children ?? <SharedAppBar />}
    </WebBaseGateway>
  );
};

NativeWebAppBar.displayName = 'NativeWebAppBar';

export default NativeWebAppBar;
