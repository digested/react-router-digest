import {
  Providers as BaseProviders,
  type ProvidersProps as BaseProvidersProps
} from '@dgui/react-shared';
import {
  WebBaseGatewayProvider
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  drawerIndex,
  drawerOpen,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  tabIndex
}) => {
  return (
    <BaseProviders
      drawerIndex={drawerIndex}
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
      onSelectDrawer={onSelectDrawer}
      onSelectTab={onSelectTab}
      tabIndex={tabIndex}
    >
      <WebBaseGatewayProvider>
        {children}
      </WebBaseGatewayProvider>
    </BaseProviders>
  );
};

Providers.displayName = 'NativeWebProviders';

export default Providers;

export type ProvidersProps = BaseProvidersProps & PropsWithChildren<object>;
