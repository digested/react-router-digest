/*
  eslint-disable
  unicorn/prevent-abbreviations
*/

import {
  type GatewayDestProps
} from '@dgui/react-shared';
import {
  WebBaseGatewayContext
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  useContext,
  useEffect,
  useRef
} from 'react';
import {
  View,
  type ViewStyle

} from 'react-native';

const NativeWebGatewayDest: FunctionComponent<NativeWebGatewayDestProps> = ({
  gatewayId,
  style
}) => {
  const ref = useRef<null | View>(null);

  const gatewayRegistry = useContext(WebBaseGatewayContext);

  useEffect(
    () => {
      gatewayRegistry?.addRef(
        gatewayId,
        ref
      );

      return () => {
        gatewayRegistry?.removeRef(gatewayId);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <View
      ref={ref}
      style={style}
    />
  );
};

NativeWebGatewayDest.displayName = 'NativeWebGatewayDest';

export default NativeWebGatewayDest;

export type NativeWebGatewayDestProps = GatewayDestProps & {
  readonly style?: ViewStyle;
};
