import Drawers from './Components/Drawers';
import Tabs from './Components/Tabs';
import {
  NativeWebGatewayDest as GatewayDestination
} from './Generics/NativeWebGateway';
import Providers from './Providers';
import {
  type ReactNativeWebDigestProps
} from './Types';
import {
  type FunctionComponent
} from 'react';
import {
  StyleSheet,
  type ViewStyle
} from 'react-native';

const styles = StyleSheet.create({
  // zIndex: 1
  appbar: {}
});

const ReactNativeWebDigest: FunctionComponent<ReactNativeWebDigestProps> = ({
  children,
  drawerAnimationTime,
  drawerIndex,
  drawerOpacity,
  drawerOpen,
  drawerPosition,
  drawerStyle,
  drawerWidth,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  swipe,
  tabIndex
}) => {
  return (
    <Providers
      drawerIndex={drawerIndex}
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
      onSelectDrawer={onSelectDrawer}
      onSelectTab={onSelectTab}
      tabIndex={tabIndex}
    >
      <GatewayDestination
        gatewayId='ReactNativeWebDigestAppBar'
        style={styles.appbar}
      />
      <Drawers
        animationTime={drawerAnimationTime}
        opacity={drawerOpacity}
        position={drawerPosition}
        style={drawerStyle as ViewStyle}
        width={drawerWidth}
      >
        <Tabs
          swipe={swipe}
        />
      </Drawers>
      {children}
    </Providers>
  );
};

ReactNativeWebDigest.displayName = 'ReactNativeWebDigest';

export default ReactNativeWebDigest;
