import {
  ReactNativeDigestDrawerBar
} from '@dgui/react-native';
import {
  ReactNativeMaterialSharedDrawerBar,
  type ReactNativeMaterialSharedDrawerBarProps
} from '@dgui/react-native-material-bars-shared';
import {
  type FunctionComponent
} from 'react';

const DrawerBar: FunctionComponent<DrawerBarProps> = (
  props
) => {
  return (
    <ReactNativeDigestDrawerBar>
      <ReactNativeMaterialSharedDrawerBar
        {...props}
      />
    </ReactNativeDigestDrawerBar>
  );
};

export default DrawerBar;

export type DrawerBarProps = ReactNativeMaterialSharedDrawerBarProps;
