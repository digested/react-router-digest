import {
  ReactNativeDigestAppBar
} from '@dgui/react-native';
import {
  ReactNativeMaterialSharedAppBar,
  type ReactNativeMaterialSharedAppBarProps
} from '@dgui/react-native-material-bars-shared';
import {
  type FunctionComponent
} from 'react';

const AppBar: FunctionComponent<AppBarProps> = (
  props
) => {
  return (
    <ReactNativeDigestAppBar>
      <ReactNativeMaterialSharedAppBar
        {...props}
      />
    </ReactNativeDigestAppBar>
  );
};

export default AppBar;

export type AppBarProps = ReactNativeMaterialSharedAppBarProps;
