import {
  NativeGatewayProvider
} from '../Generics/NativeGateway';
import {
  Providers as BaseProviders,
  type ProvidersProps as BaseProvidersProps
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  drawerIndex,
  drawerOpen,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  tabIndex
}) => {
  return (
    <BaseProviders
      drawerIndex={drawerIndex}
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
      onSelectDrawer={onSelectDrawer}
      onSelectTab={onSelectTab}
      tabIndex={tabIndex}
    >
      <NativeGatewayProvider>
        {children}
      </NativeGatewayProvider>
    </BaseProviders>
  );
};

Providers.displayName = 'NativeProviders';

export default Providers;

export type ProvidersProps = BaseProvidersProps & PropsWithChildren<object>;
