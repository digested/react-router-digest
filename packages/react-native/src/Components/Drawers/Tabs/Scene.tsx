/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prevent-abbreviations
*/

import {
  NativeHorse
} from '../../../Generics/NativeCarousel';
import {
  NativeGatewayDest
} from '../../../Generics/NativeGateway';
import Context from './Context';
import {
  useDrawers
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = ({
  drawerId
}) => {
  const drawers = useDrawers();

  const {
    swipe,
    tabs
  } = drawers[drawerId];

  const gatewayId = useMemo(
    () => {
      return `drawer-${drawerId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <NativeHorse
      swipe={swipe}
    >
      {
        tabs ?
        // eslint-disable-next-line @stylistic/no-extra-parens
          (
            <Context
              drawerId={drawerId}
            />
          ) :
          // eslint-disable-next-line @stylistic/no-extra-parens
          (
            <NativeGatewayDest
              gatewayId={gatewayId}
            />
          )

      }
    </NativeHorse>
  );
};

Scene.displayName = 'NativeDrawerTabsScene';

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly drawerId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
