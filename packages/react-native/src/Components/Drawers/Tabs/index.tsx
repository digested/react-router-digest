import {
  NativeCarousel
} from '../../../Generics/NativeCarousel';
import Scene from './Scene';
import {
  useDrawerIndex,
  useDrawerOrder,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Drawers: FunctionComponent<DrawerProps> = () => {
  const drawerIndex = useDrawerIndex();
  const selectDrawer = useSelectDrawer();
  const drawerOrder = useDrawerOrder();

  const drawers = useMemo(
    () => {
      return drawerOrder.map(
        (drawerId) => {
          return (
            <Scene
              drawerId={drawerId}
              key={drawerId}
            />
          );
        }
      );
    },
    [
      drawerOrder
    ]
  );

  return (
    <NativeCarousel
      onSelectTab={selectDrawer}
      tabIndex={drawerIndex}
    >
      {drawers}
    </NativeCarousel>
  );
};

Drawers.displayName = 'NativeDrawerTabs';

export default memo(Drawers);

export type DrawerProps = object;
