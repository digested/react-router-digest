/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prevent-abbreviations
*/

import {
  NativeHorse
} from '../../../../Generics/NativeCarousel';
import {
  NativeGatewayDest
} from '../../../../Generics/NativeGateway';
import {
  useTabs
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = (
  {
    drawerId,
    tabId
  }
) => {
  const tabs = useTabs();

  const {
    swipe
  } = tabs[tabId];

  const gatewayId = useMemo(
    () => {
      return `drawer-${drawerId}-${tabId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <NativeHorse
      swipe={swipe}
    >
      <NativeGatewayDest
        gatewayId={gatewayId}
      />
    </NativeHorse>
  );
};

Scene.displayName = 'NativeDrawerTabsSceneContextScene';

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly drawerId: string;
  readonly tabId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
