import {
  NativeGatewayDest as GatewayDestination
} from '../../Generics/NativeGateway';
import {
  type ReactNativeDigestDrawersProps
} from '../../Types';
import Tabs from './Tabs';
import {
  NativeBaseDrawers as SideDrawers
} from '@dgui/react-native-shared';
import {
  useDrawerOpen
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';
import {
  type ViewStyle

} from 'react-native';

const Drawers: FunctionComponent<DrawersProps> = (
  props
) => {
  const drawerOpen = useDrawerOpen();

  const appBar = useMemo(
    () => {
      return (
        <>
          <GatewayDestination
            gatewayId='ReactNativeDigestDrawerBar'
          />
          <Tabs />
        </>
      );
    },
    []
  );

  return (
    <SideDrawers
      drawerContent={appBar}
      open={drawerOpen}
      {...props}
    />
  );
};

Drawers.displayName = 'NativeDrawers';

export default Drawers;

export type DrawersProps = Omit<PropsWithChildren<ReactNativeDigestDrawersProps>, 'style'> & {
  readonly style: ViewStyle;
};
