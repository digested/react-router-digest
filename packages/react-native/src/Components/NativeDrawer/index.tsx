import {
  NativeGateway
} from '../../Generics/NativeGateway';
import {
  type ReactNativeDigestDrawerProps
} from '../../Types';
import {
  useAddDrawer,
  useRemoveDrawer
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const NativeDrawer: FunctionComponent<ReactNativeDigestDrawerProps> = ({
  children,
  ...props
}) => {
  // Tab ID should never change
  // Proper lifecycle is to unmount the component and add new with new ID
  const {
    drawerId,
    gatewayId
  } = useMemo(
    () => {
      return {
        drawerId: props.drawerId,
        gatewayId: props.tabId ?
          `drawer-${props.drawerId}-${props.tabId}` :
          `drawer-${props.drawerId}`
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const addDrawer = useAddDrawer();
  const removeDrawer = useRemoveDrawer();

  useEffect(
    () => {
      addDrawer(props);

      return () => {
        removeDrawer(
          drawerId
        );
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <NativeGateway
      gatewayId={gatewayId}
    >
      {children}
    </NativeGateway>
  );
};

NativeDrawer.displayName = 'NativeDrawer';

export default NativeDrawer;
