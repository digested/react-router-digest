import {
  NativeGateway
} from '../../Generics/NativeGateway';
import {
  type ReactNativeDigestTabProps
} from '../../Types';
import {
  useAddTab,
  useRemoveTab
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const NativeTab: FunctionComponent<ReactNativeDigestTabProps> = ({
  children,
  ...props
}) => {
  // Tab ID should never change
  // Proper lifecycle is to unmount the component and add new with new ID
  const {
    gatewayId,
    tabId
  } = useMemo(
    () => {
      return {
        gatewayId: `tab-${props.tabId}`,
        tabId: props.tabId
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const addTab = useAddTab();
  const removeTab = useRemoveTab();

  useEffect(
    () => {
      addTab(props);

      return () => {
        removeTab(
          tabId
        );
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <NativeGateway
      gatewayId={gatewayId}
    >
      {children}
    </NativeGateway>
  );
};

NativeTab.displayName = 'NativeTab';

export default NativeTab;
