import {
  NativeGateway
} from '../../Generics/NativeGateway';
import {
  type ReactNativeDigestDrawerBarProps
} from '../../Types';
import {
  DrawerBar as SharedDrawerBar
} from '@dgui/react-native-shared';
import {
  type FunctionComponent
} from 'react';

const NativeDrawerBar: FunctionComponent<ReactNativeDigestDrawerBarProps> = ({
  children
}) => {
  return (
    <NativeGateway
      gatewayId='ReactNativeDigestDrawerBar'
    >
      {children ?? <SharedDrawerBar />}
    </NativeGateway>
  );
};

NativeDrawerBar.displayName = 'NativeDrawerBar';

export default NativeDrawerBar;
