/*
  eslint-disable
  canonical/filename-match-exported
*/

import {
  NativeHorse
} from '../../Generics/NativeCarousel';
import {
  NativeGatewayDest as GatewayDestination
} from '../../Generics/NativeGateway';
import {
  useTabs
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = ({
  tabId
}) => {
  const tabs = useTabs();

  const {
    swipe
  } = tabs[tabId];

  const gatewayId = useMemo(
    () => {
      return `tab-${tabId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <NativeHorse
      swipe={swipe}
    >
      <GatewayDestination
        gatewayId={gatewayId}
      />
    </NativeHorse>
  );
};

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly tabId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
