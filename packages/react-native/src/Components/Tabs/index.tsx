import {
  NativeCarousel
} from '../../Generics/NativeCarousel';
import Scene from './Scene';
import {
  useSelectTab,
  useTabIndex,
  useTabOrder
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Tabs: FunctionComponent<TabProps> = ({
  swipe
}) => {
  const selectTab = useSelectTab();
  const tabIndex = useTabIndex();
  const tabOrder = useTabOrder();

  const tabs = useMemo(
    () => {
      const orderedTabs = tabOrder.map(
        (tab) => {
          return (
            <Scene
              key={tab}
              tabId={tab}
            />
          );
        }
      );

      return orderedTabs;
    },
    [
      tabOrder
    ]
  );

  return (
    <NativeCarousel
      onSelectTab={selectTab}
      swipe={swipe}
      tabIndex={tabIndex}
    >
      {tabs}
    </NativeCarousel>
  );
};

export default memo(Tabs);

export type TabProps = {
  readonly swipe?: boolean;
};
