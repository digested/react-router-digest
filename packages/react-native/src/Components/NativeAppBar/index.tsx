import {
  NativeGateway
} from '../../Generics/NativeGateway';
import {
  type ReactNativeDigestAppBarProps
} from '../../Types';
import {
  AppBar as SharedAppBar
} from '@dgui/react-native-shared';
import {
  type FunctionComponent
} from 'react';

const NativeAppBar: FunctionComponent<ReactNativeDigestAppBarProps> = ({
  children
}) => {
  return (
    <NativeGateway
      gatewayId='ReactNativeDigestAppBar'
    >
      {children ?? <SharedAppBar />}
    </NativeGateway>
  );
};

NativeAppBar.displayName = 'NativeAppBar';

export default NativeAppBar;
