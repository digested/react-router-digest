import {
  type ReactDigestAppBarProps,
  type ReactDigestDrawerBarProps,
  type ReactDigestDrawerProps,
  type ReactDigestDrawersProps,
  type ReactDigestProps,
  type ReactDigestTabProps
} from '@dgui/react-shared';

export type ReactNativeDigestAppBarProps = ReactDigestAppBarProps;

export type ReactNativeDigestDrawerBarProps = ReactDigestDrawerBarProps;

export type ReactNativeDigestDrawerProps = ReactDigestDrawerProps;

export type ReactNativeDigestDrawersProps = ReactDigestDrawersProps;

export type ReactNativeDigestProps = ReactDigestProps;

export type ReactNativeDigestTabProps = ReactDigestTabProps;
