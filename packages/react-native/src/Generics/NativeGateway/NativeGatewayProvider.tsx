import NativeGatewayContext from './NativeGatewayContext';
import {
  type Dispatch,
  type FunctionComponent,
  type PropsWithChildren,
  type ReactNode,
  type SetStateAction,
  useCallback,
  useMemo,
  useRef
} from 'react';

const NativeGatewayProvider: FunctionComponent<NativeGatewayProviderProps> = ({
  children: providerChildren
}) => {
  const currentId = useRef(0);

  const containers = useRef<{
    [containerName: string]: Dispatch<SetStateAction<React.ReactNode>>;
  }>({});

  const children = useRef<{
    [childName: string]: {
      [gatewayId: string]: ReactNode;
    };
  }>({});

  const renderContainer = useCallback(
    (
      name: string
    ) => {
      if (!containers.current[name] || !children.current[name]) {
        return;
      }

      containers.current[name](
        () => {
          // eslint-disable-next-line @typescript-eslint/require-array-sort-compare
          const newChild = Object
            .keys(children.current[name])
            .sort()
            .map(
              // eslint-disable-next-line @typescript-eslint/promise-function-async
              (id) => {
                return children.current[name][id];
              }
            );

          return newChild;
        }
      );
    },
    []
  );

  const addChild = useCallback(
    (
      name: string,
      gatewayId: string,
      child: ReactNode
    ) => {
      const newChildren = {
        ...children.current,
        [name]: {
          ...children.current[gatewayId],
          [gatewayId]: child
        }
      };

      children.current = newChildren;

      renderContainer(name);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const addContainer = useCallback(
    (
      name: string,
      child: Dispatch<SetStateAction<ReactNode>>
    ) => {
      const newContainers = {
        ...containers.current,
        [name]: child
      };

      containers.current = newContainers;

      renderContainer(name);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const clearChild = useCallback(
    (
      name: string,
      gatewayId: string
    ) => {
      const {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        [gatewayId]: omit,
        ...gateways
      } = children.current[name];

      const clearedChildren = {
        ...children.current,
        [name]: gateways
      };

      children.current = clearedChildren;
    },
    []
  );

  const register = useCallback(
    (
      name: string,
      child: ReactNode
    ) => {
      const gatewayId = `${name}_${currentId.current}`;

      const registeredChildren = {
        ...children.current,
        [name]: {
          ...children.current[name],
          [gatewayId]: child
        }
      };

      children.current = registeredChildren;

      currentId.current += 1;

      return gatewayId;
    },
    []
  );

  const removeContainer = useCallback(
    (
      name: string
    ) => {
      const {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        [name]: omit,
        ...reducedContainers
      } = containers.current;

      containers.current = reducedContainers;
    },
    []
  );

  const unregister = useCallback(
    (
      name: string,
      gatewayId: string
    ) => {
      clearChild(
        name,
        gatewayId
      );

      renderContainer(name);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const value = useMemo(
    () => {
      return {
        addChild,
        addContainer,
        clearChild,
        register,
        removeContainer,
        unregister
      };
    },
    [
      addChild,
      addContainer,
      clearChild,
      register,
      removeContainer,
      unregister
    ]
  );

  return (
    <NativeGatewayContext.Provider
      value={value}
    >
      {providerChildren}
    </NativeGatewayContext.Provider>
  );
};

NativeGatewayProvider.displayName = 'NativeGatewayProvider';

export default NativeGatewayProvider;

export type NativeGatewayProviderProps = PropsWithChildren<object>;
