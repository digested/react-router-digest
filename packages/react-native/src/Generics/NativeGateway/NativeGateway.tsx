import NativeGatewayContext from './NativeGatewayContext';
import {
  type GatewayProps
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  type ReactNode,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef
} from 'react';

const NativeGateway: FunctionComponent<NativeGatewayProps> = ({
  children,
  gatewayId: givenGatewayId
}) => {
  const gatewayId = useMemo(
    () => {
      return givenGatewayId;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const id = useRef<string>('');

  const gatewayRegistry = useContext(NativeGatewayContext);

  const renderIntoGatewayNode = useCallback(
    (
      in2: string,
      gwId: string,
      child: ReactNode
    ) => {
      if (gatewayRegistry) {
        gatewayRegistry.addChild(
          in2,
          gwId,
          child
        );
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useEffect(
    () => {
      if (gatewayRegistry) {
        id.current = gatewayRegistry.register(
          gatewayId,
          children
        );
      }

      renderIntoGatewayNode(
        gatewayId,
        id.current,
        children
      );

      return () => {
        if (gatewayRegistry) {
          gatewayRegistry.unregister(
            gatewayId,
            id.current
          );
        }
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useEffect(
    () => {
      if (gatewayRegistry) {
        gatewayRegistry.clearChild(
          gatewayId,
          id.current
        );
      }

      renderIntoGatewayNode(
        gatewayId,
        id.current,
        children
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      children,
      gatewayId
    ]
  );

  return null;
};

NativeGateway.displayName = 'NativeGateway';

export default NativeGateway;

export type NativeGatewayProps = GatewayProps;
