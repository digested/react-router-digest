import {
  createContext,
  type Dispatch,
  type ReactNode,
  type SetStateAction
} from 'react';

const NativeGatewayContext = createContext<NativeGatewayContextProps | null>(null);

NativeGatewayContext.displayName = 'NativeGatewayContext';

export default NativeGatewayContext;

export type NativeGatewayContextProps = {
  addChild: (
    name: string,
    gatewayId: string,
    child: ReactNode
  ) => void;
  addContainer: (
    name: string,
    container: Dispatch<SetStateAction<ReactNode>>
  ) => void;
  clearChild: (
    name: string,
    gatewayId: string
  ) => void;
  register: (
    name: string,
    child: React.ReactNode
  ) => string;
  removeContainer: (
    name: string
  ) => void;
  unregister: (
    name: string,
    gatewayId: string
  ) => void;
};
