/*
  eslint-disable
  unicorn/prevent-abbreviations
*/

import NativeGatewayContext from './NativeGatewayContext';
import {
  type GatewayDestProps
} from '@dgui/react-shared';
import {
  createElement,
  type FunctionComponent,
  type ReactNode,
  useContext,
  useEffect,
  useState
} from 'react';
import {
  View,
  type ViewStyle

} from 'react-native';

const NativeGatewayDest: FunctionComponent<NativeGatewayDestProps> = ({
  gatewayId,
  ...attrs
}) => {
  const [
    children,
    setChildren
  ] = useState<ReactNode>(null);

  const gatewayRegistry = useContext(NativeGatewayContext);

  useEffect(
    () => {
      setChildren(
        // eslint-disable-next-line @typescript-eslint/promise-function-async
        (currentChildren) => {
          return currentChildren;
        }
      );
    },
    [
      children
    ]
  );

  useEffect(
    () => {
      if (gatewayRegistry) {
        gatewayRegistry.addContainer(
          gatewayId,
          setChildren
        );
      }

      return () => {
        if (gatewayRegistry) {
          gatewayRegistry.removeContainer(
            gatewayId
          );
        }
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return createElement(
    View,
    attrs,
    children
  );
};

NativeGatewayDest.displayName = 'NativeGatewayDest';

export default NativeGatewayDest;

export type NativeGatewayDestProps = GatewayDestProps & {
  readonly style?: ViewStyle;
};
