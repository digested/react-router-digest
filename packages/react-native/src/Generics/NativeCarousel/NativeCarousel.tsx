import {
  type CarouselProps
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  type NativeSyntheticEvent,
  StyleSheet,
  type ViewStyle

} from 'react-native';
import PagerView, {
  type PagerViewOnPageScrollEvent
} from 'react-native-pager-view';

const PAGE_MARGIN = 0;
const OFFSCREEN_LIMIT = 99;
const OVERSCROLL_MODE = 'always';
const OVERDRAG = true;

const styles = StyleSheet.create({
  pagerView: {
    flex: 1
  }
});

type PageScrollStates = 'dragging' | 'idle' | 'settling';

const NativeCarousel: FunctionComponent<CarouselProps> = ({
  children,
  onSelectTab,
  style: givenStyle,
  swipe,
  tabIndex = 0
}) => {
  const pager = useRef<null | PagerView>(null);
  const pagePosition = useRef<number>(tabIndex);

  const handlePageScroll = useCallback(
    (
      event: PagerViewOnPageScrollEvent
    ) => {
      pagePosition.current = event.nativeEvent.position;
    },
    []
  );

  const style = useMemo(
    () => {
      return {
        ...styles.pagerView,
        ...givenStyle
      };
    },
    [
      givenStyle
    ]
  );

  const handlePageScrollStateChanged = useCallback(
    (
      event: NativeSyntheticEvent<Readonly<{ pageScrollState: PageScrollStates }>>
    ) => {
      const {
        pageScrollState
      } = event.nativeEvent;

      onSelectTab(
        (
          currentTabIndex
        ) => {
          if (
            pagePosition.current !== currentTabIndex &&
            pageScrollState === 'idle'
          ) {
            return pagePosition.current;
          } else {
            return currentTabIndex;
          }
        }
      );
    },
    [
      onSelectTab
    ]
  );

  useEffect(
    () => {
      if (
        pager.current
      ) {
        pager.current.setPage(tabIndex);
      }
    },
    [
      tabIndex
    ]
  );

  return (
    <PagerView
      initialPage={tabIndex}
      offscreenPageLimit={OFFSCREEN_LIMIT}
      onPageScroll={handlePageScroll}
      onPageScrollStateChanged={handlePageScrollStateChanged}
      overdrag={OVERDRAG}
      overScrollMode={OVERSCROLL_MODE}
      pageMargin={PAGE_MARGIN}
      ref={pager}
      scrollEnabled={swipe ?? true}
      style={style as ViewStyle}
    >
      {children}
    </PagerView>
  );
};

NativeCarousel.displayName = 'NativeCarousel';

export default NativeCarousel;

export type NativeCarouselProps = CarouselProps;
