import {
  type HorseProps
} from '@dgui/react-shared';
import {
  type FunctionComponent
} from 'react';

const NativeHorse: FunctionComponent<NativeHorseProps> = ({
  children
}) => {
  return (
    <>
      {children}
    </>
  );
};

NativeHorse.displayName = 'NativeHorse';

export default NativeHorse;

export type NativeHorseProps = HorseProps;
