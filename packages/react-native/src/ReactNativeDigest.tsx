import Drawers from './Components/Drawers';
import Tabs from './Components/Tabs';
import {
  NativeGatewayDest as GatewayDestination
} from './Generics/NativeGateway';
import Providers from './Providers';
import {
  type ReactNativeDigestProps
} from './Types';
import {
  type FunctionComponent
} from 'react';
import {
  StyleSheet,
  type ViewStyle

} from 'react-native';

const styles = StyleSheet.create({
  // zIndex: 1
  appbar: {}
});

const ReactNativeDigest: FunctionComponent<ReactNativeDigestProps> = ({
  children,
  drawerAnimationTime,
  drawerIndex,
  drawerOpacity,
  drawerOpen,
  drawerPosition,
  drawerStyle,
  drawerWidth,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  swipe,
  tabIndex
}) => {
  return (
    <Providers
      drawerIndex={drawerIndex}
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
      onSelectDrawer={onSelectDrawer}
      onSelectTab={onSelectTab}
      tabIndex={tabIndex}
    >
      <GatewayDestination
        gatewayId='ReactNativeDigestAppBar'
        style={styles.appbar}
      />
      <Drawers
        animationTime={drawerAnimationTime}
        opacity={drawerOpacity}
        position={drawerPosition}
        style={drawerStyle as ViewStyle}
        width={drawerWidth}
      >
        <Tabs
          swipe={swipe}
        />
      </Drawers>
      {children}
    </Providers>
  );
};

ReactNativeDigest.displayName = 'ReactNativeDigest';

export default ReactNativeDigest;
