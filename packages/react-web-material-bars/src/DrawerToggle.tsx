import {
  useDrawerToggle
} from '@dgui/react-shared';
import MenuIcon from '@mui/icons-material/Menu';
import IconButton from '@mui/material/IconButton';
import {
  type CSSProperties,
  type FunctionComponent,
  memo,
  useCallback
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  iconButton: {
    margin: 0
  }
};

const DrawerToggle: FunctionComponent<DrawerToggleProps> = ({
  icon: Icon
}) => {
  const toggleDrawer = useDrawerToggle();

  const onClick = useCallback(
    () => {
      if (toggleDrawer) {
        toggleDrawer();
      }
    },
    [
      toggleDrawer
    ]
  );

  return (
    <IconButton
      aria-label='open drawer'
      onClick={onClick}
      style={styles.iconButton}
    >
      {
        Icon ?
          <Icon /> :
          <MenuIcon />
      }
    </IconButton>
  );
};

export default memo(
  DrawerToggle
);

export type DrawerToggleProps = {
  readonly icon?: FunctionComponent;
};
