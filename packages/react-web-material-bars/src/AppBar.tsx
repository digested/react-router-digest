import DrawerToggle from './DrawerToggle';
import Tabs from './Tabs';
import {
  useSelectTab,
  useTabIndex,
  useTabOrder,
  useTabs
} from '@dgui/react-shared';
import {
  ReactWebDigestAppBar
} from '@dgui/react-web';
import {
  type CSSProperties,
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  appBar: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    boxShadow: '0 0 10px 5px rgba(0, 0, 0, 0.1)',
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    zIndex: 102
  }
};

const AppBar: FunctionComponent<AppBarProps> = ({
  appBarStyle: givenAppBarStyle,
  children,
  icon,
  tabBarStyle,
  ...props
}) => {
  const appBarStyle: CSSProperties = useMemo(
    () => {
      return {
        ...styles.appBar,
        ...givenAppBarStyle
      };
    },
    [
      givenAppBarStyle
    ]
  );

  const selectTab = useSelectTab();
  const tabIndex = useTabIndex();

  const handleSelectTab = useCallback(
    (
      index: number
    ) => {
      selectTab(index);
    },
    [
      selectTab
    ]
  );

  const tabOrder = useTabOrder();
  const tabs = useTabs();

  const routes = useMemo(
    () => {
      return tabOrder.map(
        (
          tabId
        ) => {
          return {
            accessibilityLabel: tabs[tabId]?.title ?? tabId,
            icon: undefined,
            key: tabId,
            title: tabs[tabId]?.title ?? tabId
          };
        }
      );
    },
    [
      tabOrder,
      tabs
    ]
  );

  return (
    <ReactWebDigestAppBar>
      <div
        style={appBarStyle}
      >
        <DrawerToggle
          icon={icon}
        />
        <Tabs
          {...props}
          defaultIndex={tabIndex}
          onSelectTab={handleSelectTab}
          routes={routes}
          style={tabBarStyle}
        />
        {children}
      </div>
    </ReactWebDigestAppBar>
  );
};

export default AppBar;

export type AppBarProps = PropsWithChildren<{
  readonly appBarStyle?: CSSProperties;
  readonly icon?: FunctionComponent;
  readonly tabBarStyle?: CSSProperties;
}>;
