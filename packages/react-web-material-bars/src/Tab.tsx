import GenericTab, {
  type TabProps as GenericTabProps
} from '@mui/material/Tab';
import {
  type FunctionComponent,
  type ReactNode
} from 'react';

const Tab: FunctionComponent<TabProps> = ({
  label,
  tabIndex,
  ...rest
}) => {
  return (
    <GenericTab
      label={label}
      style={{
        height: '20px'
      }}
      value={tabIndex}
      {...rest}
    />
  );
};

export default Tab;

export type TabProps = GenericTabProps & {
  readonly label: ReactNode;
  readonly tabIndex: number;
};
