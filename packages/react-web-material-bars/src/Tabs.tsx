import renderScene from './renderScene';
import MaterialTabs, {
  type TabsProps as MaterialTabsProps
} from '@mui/material/Tabs';
import {
  type CSSProperties,
  type FunctionComponent,
  type SyntheticEvent,
  useCallback,
  useMemo
} from 'react';

const {
  defaultTabStyle
}: {
  [key: string]: CSSProperties;
} = {
  defaultTabStyle: {
    flex: 1
  }
};

const Tabs: FunctionComponent<TabsProps> = ({
  defaultIndex,
  onSelectTab,
  routes = [],
  style: givenStyle,
  ...props
}) => {
  const tabStyle = useMemo(
    () => {
      return {
        ...defaultTabStyle,
        ...givenStyle
      };
    },
    [
      givenStyle
    ]
  );

  const handleWebSelectTab = useCallback(
    (
      event: SyntheticEvent,
      tabIndex: number
    ) => {
      if (onSelectTab) {
        onSelectTab(tabIndex);
      }
    },
    [
      onSelectTab
    ]
  );

  const renderedScene = useMemo(
    () => {
      return renderScene(routes);
    },
    [
      routes
    ]
  );

  return (
    <MaterialTabs
      {...props}
      onChange={handleWebSelectTab}
      style={tabStyle}
      value={defaultIndex}
    >
      {renderedScene}
    </MaterialTabs>
  );
};

export default Tabs;

export type TabsProps = MaterialTabsProps & {
  readonly defaultIndex: number;
  readonly onSelectTab?: (
    tabIndex: number
  ) => void;
  readonly routes?: Array<{
    key: string;
    title: string;
  }>;
};
