import Tabs from './Tabs';
import {
  useDrawerIndex,
  useDrawerOrder,
  useDrawers,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  ReactWebDigestDrawerBar
} from '@dgui/react-web';
import {
  type CSSProperties,
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  tabBar: {
    backgroundColor: '#CCC',
    // boxShadow: '0 0 10px 5px rgba(0, 0, 0, 0.1)',
    display: 'flex',
    flexDirection: 'row'
  }
};

const DrawerBar: FunctionComponent<DrawerBarProps> = () => {
  const drawerIndex = useDrawerIndex();
  const selectDrawer = useSelectDrawer();

  const handleSelectDrawer = useCallback(
    (
      tabIndex: number
    ) => {
      selectDrawer(tabIndex);
    },
    [
      selectDrawer
    ]
  );

  const drawerOrder = useDrawerOrder();
  const drawers = useDrawers();

  const routes = useMemo(
    () => {
      return drawerOrder.map(
        (
          drawerId
        ) => {
          return (
            {
              key: drawerId,
              title: drawers[drawerId]?.title ?? drawerId
            }
          );
        }
      );
    },
    [
      drawerOrder,
      drawers
    ]
  );

  return (
    <ReactWebDigestDrawerBar>
      <div
        style={styles.tabBar}
      >
        <Tabs
          defaultIndex={drawerIndex}
          onSelectTab={handleSelectDrawer}
          routes={routes}
        />
      </div>
    </ReactWebDigestDrawerBar>
  );
};

export default DrawerBar;

export type DrawerBarProps = object;
