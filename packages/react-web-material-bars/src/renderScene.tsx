import Tab from './Tab';
import {
  type CSSProperties
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  tab: {
    textTransform: 'initial'
  }
};

export default (
  routes: Array<{
    key: string;
    title: string;
  }>
) => {
  return routes.map(
    (
      {
        key,
        title
      },
      index
    ) => {
      return (
        <Tab
          key={key}
          label={title}
          style={styles.tab}
          tabIndex={index}
        />
      );
    }
  );
};
