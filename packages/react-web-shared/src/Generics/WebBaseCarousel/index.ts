export {
  default as WebBaseCarousel,
  type WebBaseCarouselProps
} from './WebBaseCarousel';
export {
  default as WebBaseHorse,
  type WebBaseHorseProps
} from './WebBaseHorse';
