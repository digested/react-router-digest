import {
  type CarouselProps
} from '@dgui/react-shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  Swiper
} from 'swiper/react';
import {
  type SwiperModule
} from 'swiper/types/shared';
import type SwiperClass from 'swiper/types/swiper-class';

const SLIDES_PER_VIEW = 1;
const SPACE_BETWEEN = 0;

const styles = {
  slideContainer: {
    display: 'flex',
    height: '100%',
    width: '100%'
  }
};

const modules: SwiperModule[] = [];

const WebBaseCarousel: FunctionComponent<WebBaseCarouselProps> = ({
  children,
  onSelectTab,
  style: givenStyle,
  swipe,
  tabIndex = 0
}) => {
  const style = useMemo(
    () => {
      return {
        ...styles.slideContainer,
        ...givenStyle
      };
    },
    [
      givenStyle
    ]
  );

  const handleSlideChange = useCallback(
    (
      swiper: SwiperClass
    ) => {
      if (onSelectTab) {
        onSelectTab(swiper.activeIndex);
      }
    },

    [
      onSelectTab
    ]
  );

  const swiperInstance = useRef<null | SwiperClass>(null);

  const handleSwiper = useCallback(
    (
      swiper: SwiperClass
    ) => {
      swiperInstance.current = swiper;
    },
    []
  );

  useEffect(
    () => {
      if (swiperInstance.current) {
        swiperInstance.current.slideTo(tabIndex);
      }
    },
    [
      tabIndex
    ]
  );

  return (
    <Swiper
      allowTouchMove={swipe ?? true}
      initialSlide={tabIndex}
      modules={modules}
      onSlideChange={handleSlideChange}
      onSwiper={handleSwiper}
      slidesPerView={SLIDES_PER_VIEW}
      spaceBetween={SPACE_BETWEEN}
      style={style as CSSProperties}
    >
      {children}
    </Swiper>
  );
};

WebBaseCarousel.displayName = 'WebBaseCarousel';

export default WebBaseCarousel;

export type WebBaseCarouselProps = CarouselProps;

export {
  default as WebBaseHorse
} from './WebBaseHorse';
