import {
  type HorseProps
} from '@dgui/react-shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useMemo
} from 'react';
import {
  SwiperSlide
} from 'swiper/react';

const styles = {
  horse: {
    display: 'flex',
    height: '100%',
    width: '100%'
  }
};

const WebBaseHorse: FunctionComponent<WebBaseHorseProps> = ({
  children,
  style: givenStyle,
  swipe
}) => {
  const style = useMemo(
    () => {
      return {
        ...styles.horse,
        ...givenStyle
      };
    },
    [
      givenStyle
    ]
  );

  const slide = swipe ?? true;

  return (
    <SwiperSlide
      className={
        slide ?
          undefined :
          'swiper-no-swiping'
      }
      style={style as CSSProperties}
    >
      {children}
    </SwiperSlide>
  );
};

WebBaseHorse.displayName = 'WebBaseHorse';

export default WebBaseHorse;

export type WebBaseHorseProps = HorseProps;
