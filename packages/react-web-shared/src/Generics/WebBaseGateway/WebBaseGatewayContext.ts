import {
  type Component,
  createContext,
  type MutableRefObject
} from 'react';

const WebBaseGatewayContext = createContext<null | WebBaseGatewayContextProps>(null);

WebBaseGatewayContext.displayName = 'WebBaseGatewayContext';

export default WebBaseGatewayContext;

export type WebBaseGatewayContextProps = {
  addRef: (
    id: string,
    ref: MutableRefObject<Component | HTMLDivElement | null>
  ) => void;
  refs: {
    [id: string]: MutableRefObject<Component | HTMLDivElement | null>;
  };
  removeRef: (
    id: string
  ) => void;
};
