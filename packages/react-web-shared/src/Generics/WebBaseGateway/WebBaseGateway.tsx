import WebBaseGatewayContext from './WebBaseGatewayContext';
import {
  type GatewayProps
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useContext
} from 'react';
import {
  createPortal
} from 'react-dom';

const WebBaseGateway: FunctionComponent<WebBaseGatewayProps> = ({
  children,
  gatewayId
}) => {
  const gatewayRegistry = useContext(WebBaseGatewayContext);

  const ref = gatewayRegistry?.refs[gatewayId]?.current;

  if (
    ref
  ) {
    return createPortal(
      children,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      ref
    );
  } else {
    return (
      <>
        {null}
      </>
    );
  }
};

WebBaseGateway.displayName = 'WebBaseGateway';

export default WebBaseGateway;

export type WebBaseGatewayProps = GatewayProps;
