/*
  eslint-disable
  unicorn/prevent-abbreviations
*/

import WebBaseGatewayContext from './WebBaseGatewayContext';
import {
  type Component,
  type FunctionComponent,
  type PropsWithChildren,
  type RefObject,
  useCallback,
  useMemo,
  useState
} from 'react';

const WebBaseGatewayProvider: FunctionComponent<WebBaseGatewayProviderProps> = ({
  children
}) => {
  const [
    refs,
    setRefs
  ] = useState<{
    [id: string]: RefObject<Component | HTMLDivElement | null>;
  }>({});

  const addRef = useCallback(
    (
      id: string,
      ref: RefObject<Component | HTMLDivElement | null>
    ) => {
      setRefs(
        (
          currentRefs
        ) => {
          return {
            ...currentRefs,
            [id]: ref
          };
        }
      );
    },
    []
  );

  const removeRef = useCallback(
    (
      id: string
    ) => {
      setRefs(
        (
          currentRefs
        ) => {
          const {
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            [id]: ref,
            ...filteredRefs
          } = currentRefs;

          return filteredRefs;
        }
      );
    },
    []
  );

  const value = useMemo(
    () => {
      return {
        addRef,
        refs,
        removeRef
      };
    },
    [
      addRef,
      refs,
      removeRef
    ]
  );

  return (
    <WebBaseGatewayContext.Provider
      value={value}
    >
      {children}
    </WebBaseGatewayContext.Provider>
  );
};

WebBaseGatewayProvider.displayName = 'WebBaseGatewayProvider';

export default WebBaseGatewayProvider;

export type WebBaseGatewayProviderProps = PropsWithChildren<object>;
