/*
  eslint-disable
  unicorn/prevent-abbreviations
*/

import {
  type CSSProperties,
  type Dispatch,
  type PropsWithChildren,
  type SetStateAction
} from 'react';
import {
  type ViewStyle
  // eslint-disable-next-line n/no-extraneous-import
} from 'react-native';

export type CarouselProps = PropsWithChildren<{
  readonly onSelectTab: Dispatch<SetStateAction<number>>;
  readonly style?: CSSProperties | ViewStyle;
  readonly swipe?: boolean;
  readonly tabIndex?: number;
}>;

export type DrawerContextProps = {
  readonly addDrawer: (
    drawer: DrawerType,
    position?: number
  ) => void;
  readonly drawerOrder: string[];
  readonly drawers: {
    [key: string]: DrawerType;
  };
  readonly removeDrawer: (
    drawerId: string
  ) => void;
};

export type DrawerIndexContextProps = {
  readonly drawerIndex: number;
  readonly selectDrawer: Dispatch<SetStateAction<number>>;
};

export type DrawerIndexProviderProps = PropsWithChildren<{
  readonly drawerIndex?: number;
  readonly onSelectDrawer?: (tabIndex: number) => void;
}>;

export type DrawerPosition = 'left' | 'right';

export type DrawerToggleContextProps = {
  readonly drawerOpen: boolean;
  readonly toggleDrawer: (
    open?: boolean
  ) => void;
};

export type DrawerToggleProviderProps = PropsWithChildren<{
  readonly drawerOpen?: boolean;
  readonly onDrawerToggle?: (drawerOpen?: boolean) => void;
}>;

export type DrawerType = {
  drawerId: string;
  swipe?: boolean;
  tabs?: string[];
  title?: string;
};

export type GatewayDestProps = {
  readonly gatewayId: string;
};

export type GatewayProps = PropsWithChildren<{
  readonly gatewayId: string;
}>;

export type HorseProps = PropsWithChildren<{
  readonly style?: CSSProperties | ViewStyle;
  readonly swipe?: boolean;
}>;

export type ProvidersProps = DrawerIndexProviderProps &
DrawerToggleProviderProps &
PropsWithChildren<object> &
TabIndexProviderProps;

export type ReactDigestAppBarProps = PropsWithChildren<object>;

export type ReactDigestDrawerBarProps = PropsWithChildren<object>;

export type ReactDigestDrawerProps = Omit<DrawerType, 'tabs'> &
PropsWithChildren<object> & {
  readonly tabId?: string;
};

export type ReactDigestDrawersProps = {
  readonly animationTime?: number;
  readonly opacity?: number;
  readonly position?: DrawerPosition;
  readonly style?: CSSProperties | ViewStyle;
  readonly width?: number;
};

export type ReactDigestProps = PropsWithChildren<{
  readonly drawerAnimationTime?: number;
  readonly drawerOpacity?: number;
  readonly drawerPosition?: DrawerPosition;
  readonly drawerStyle?: CSSProperties | ViewStyle;
  readonly drawerWidth?: number;
  readonly swipe?: boolean;
}> & ProvidersProps;

export type ReactDigestTabProps = PropsWithChildren<object> &
TabType;

export type TabContextProps = {
  readonly addTab: (
    tab: TabType,
    goto?: boolean,
    position?: number
  ) => void;
  readonly removeTab: (
    tabId: string
  ) => void;
  readonly selectedTabId: string;
  readonly tabOrder: string[];
  readonly tabs: {
    [key: string]: TabType;
  };
  readonly updateTab: (
    tab: TabType
  ) => void;
};

export type TabIndexContextProps = {
  readonly selectTab: Dispatch<SetStateAction<number>>;
  readonly tabIndex: number;
};

export type TabIndexProviderProps = PropsWithChildren<{
  readonly onSelectTab?: (tabIndex: number) => void;
  readonly tabIndex?: number;
}>;

export type TabType = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
  swipe?: boolean;
  tabId: string;
  title?: string;
};
