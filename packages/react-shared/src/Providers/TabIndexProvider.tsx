import {
  TabIndexContext,
  tabIndexDefaults
} from '../Contexts/TabIndexContext';
import {
  type TabIndexProviderProps
} from '../Types';
import {
  type FunctionComponent,
  useEffect,
  useMemo,
  useState
} from 'react';

const TabIndexProvider: FunctionComponent<TabIndexProviderProps> = ({
  children,
  onSelectTab,
  tabIndex: givenTabIndex
}) => {
  const [
    tabIndex,
    setTabIndex
  ] = useState(tabIndexDefaults.tabIndex);

  useEffect(
    () => {
      const safeIndex = Number.isSafeInteger(givenTabIndex) &&
        Number(givenTabIndex);

      if (
        safeIndex
      ) {
        setTabIndex(safeIndex);
      }
    },
    [
      givenTabIndex
    ]
  );

  useEffect(
    () => {
      if (onSelectTab) {
        onSelectTab(tabIndex);
      }
    },
    [
      onSelectTab,
      tabIndex
    ]
  );

  const value = useMemo(
    () => {
      return {
        selectTab: setTabIndex,
        tabIndex
      };
    },
    [
      setTabIndex,
      tabIndex
    ]
  );

  return (
    <TabIndexContext.Provider
      value={value}
    >
      {children}
    </TabIndexContext.Provider>
  );
};

export default TabIndexProvider;

TabIndexProvider.displayName = 'TabIndexProvider';
