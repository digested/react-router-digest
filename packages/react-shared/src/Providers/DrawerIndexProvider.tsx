import {
  DrawerIndexContext,
  drawerIndexDefaults
} from '../Contexts';
import {
  type DrawerIndexProviderProps
} from '../Types';
import {
  type FunctionComponent,
  useEffect,
  useMemo,
  useState
} from 'react';

const DrawerIndexProvider: FunctionComponent<DrawerIndexProviderProps> = ({
  children,
  drawerIndex: givenDrawerIndex,
  onSelectDrawer
}) => {
  const [
    drawerIndex,
    setDrawerIndex
  ] = useState(drawerIndexDefaults.drawerIndex);

  useEffect(
    () => {
      const safeIndex = Number.isSafeInteger(givenDrawerIndex) &&
        Number(givenDrawerIndex);

      if (
        safeIndex
      ) {
        setDrawerIndex(safeIndex);
      }
    },
    [
      givenDrawerIndex
    ]
  );

  useEffect(
    () => {
      if (onSelectDrawer) {
        onSelectDrawer(drawerIndex);
      }
    },
    [
      drawerIndex,
      onSelectDrawer
    ]
  );

  const value = useMemo(
    () => {
      return {
        drawerIndex,
        selectDrawer: setDrawerIndex
      };
    },
    [
      drawerIndex,
      setDrawerIndex
    ]
  );

  return (
    <DrawerIndexContext.Provider
      value={value}
    >
      {children}
    </DrawerIndexContext.Provider>
  );
};

DrawerIndexProvider.displayName = 'DrawerIndexProvider';

export default DrawerIndexProvider;
