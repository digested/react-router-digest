import {
  type ProvidersProps
} from '../Types';
import DrawerIndexProvider from './DrawerIndexProvider';
import DrawerProvider from './DrawerProvider';
import DrawerToggleProvider from './DrawerToggleProvider';
import TabIndexProvider from './TabIndexProvider';
import TabProvider from './TabProvider';
import {
  type FunctionComponent
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  drawerIndex,
  drawerOpen,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  tabIndex
}) => {
  return (
    <DrawerToggleProvider
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
    >
      <DrawerIndexProvider
        drawerIndex={drawerIndex}
        onSelectDrawer={onSelectDrawer}
      >
        <DrawerProvider>
          <TabIndexProvider
            onSelectTab={onSelectTab}
            tabIndex={tabIndex}
          >
            <TabProvider>
              {children}
            </TabProvider>
          </TabIndexProvider>
        </DrawerProvider>
      </DrawerIndexProvider>
    </DrawerToggleProvider>
  );
};

Providers.displayName = 'SharedProviders';

export {
  Providers
};
