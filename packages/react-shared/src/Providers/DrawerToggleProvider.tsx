import {
  DrawerToggleContext
} from '../Contexts';
import {
  type DrawerToggleProviderProps
} from '../Types';
import {
  type FunctionComponent,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';

const DrawerToggleProvider: FunctionComponent<DrawerToggleProviderProps> = ({
  children,
  drawerOpen: givenDrawerOpen,
  onDrawerToggle
}) => {
  const [
    drawerOpen,
    setDrawerOpen
  ] = useState(givenDrawerOpen);

  useEffect(
    () => {
      setDrawerOpen(givenDrawerOpen);
    },
    [
      givenDrawerOpen
    ]
  );

  const toggleDrawer = useCallback(
    (
      open?: boolean
    ) => {
      if (open === undefined) {
        setDrawerOpen(
          (currentDrawerOpen) => {
            const toggleOpen = !currentDrawerOpen;

            if (onDrawerToggle) {
              onDrawerToggle(toggleOpen);
            }

            return toggleOpen;
          }
        );
      } else {
        if (onDrawerToggle) {
          onDrawerToggle(open);
        }

        setDrawerOpen(open);
      }
    },
    [
      onDrawerToggle
    ]
  );

  const value = useMemo(
    () => {
      return {
        drawerOpen: Boolean(drawerOpen),
        toggleDrawer
      };
    },
    [
      drawerOpen,
      toggleDrawer
    ]
  );

  return (
    <DrawerToggleContext.Provider
      value={value}
    >
      {children}
    </DrawerToggleContext.Provider>
  );
};

DrawerToggleProvider.displayName = 'DrawerToggleProvider';

export default DrawerToggleProvider;
