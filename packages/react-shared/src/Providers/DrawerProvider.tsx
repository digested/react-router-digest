import {
  DrawerContext,
  drawerDefaults
} from '../Contexts';
import {
  type ReactDigestDrawerProps
} from '../Types';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo,
  useState
} from 'react';

const DrawerProvider: FunctionComponent<DrawerProviderProps> = ({
  children
}) => {
  const [
    drawers,
    setDrawers
  ] = useState(drawerDefaults.drawers);

  const [
    drawerOrder,
    setDrawerOrder
  ] = useState(drawerDefaults.drawerOrder);

  const addDrawer = useCallback(
    (
      drawer: ReactDigestDrawerProps,
      position?: number
    ) => {
      const {
        drawerId,
        tabId,
        ...restOfDrawer
      } = drawer;

      setDrawers(
        (previousDrawers) => {
          const existingDrawer = previousDrawers[drawerId];

          if (
            existingDrawer
          ) {
            if (tabId) {
              const newDrawers = {
                ...previousDrawers,
                [drawerId]: {
                  ...existingDrawer,
                  tabs: [
                    ...existingDrawer.tabs ?? [],
                    tabId
                  ]
                }
              };

              return newDrawers;
            } else {
              return previousDrawers;
            }
          } else if (tabId) {
            return {
              ...previousDrawers,
              [drawerId]: {
                drawerId,
                ...restOfDrawer,
                tabs: [
                  tabId
                ]
              }
            };
          } else {
            return {
              ...previousDrawers,
              [drawerId]: {
                ...drawer
              }
            };
          }
        }
      );

      setDrawerOrder(
        (previousDrawerOrder) => {
          if (
            previousDrawerOrder.includes(drawerId)
          ) {
            return previousDrawerOrder;
          } else if (
            Number.isSafeInteger(position) &&
                Number(position) > 0
          ) {
            return [
              ...previousDrawerOrder
            ].splice(
              Number(position),
              0,
              drawerId
            );
          } else {
            return [
              ...previousDrawerOrder,
              drawerId
            ];
          }
        }
      );
    },
    []
  );

  const removeDrawer = useCallback(
    (
      drawerId: string
    ) => {
      // kinda hacky but works
      let emptyDrawer = true;

      setDrawers(
        (previousDrawers) => {
          const tabs = previousDrawers[drawerId].tabs;

          if (
            tabs &&
            tabs.length !== 0
          ) {
            emptyDrawer = false;

            return previousDrawers;
          } else {
            const {
              // eslint-disable-next-line @typescript-eslint/no-unused-vars
              [drawerId]: omit,
              ...filteredDrawers
            } = previousDrawers;

            return filteredDrawers;
          }
        }
      );

      setDrawerOrder(
        (previousDrawerOrder) => {
          if (emptyDrawer) {
            return previousDrawerOrder.filter(
              (id) => {
                return id !== drawerId;
              }
            );
          } else {
            return previousDrawerOrder;
          }
        }
      );
    },
    []
  );

  const value = useMemo(
    () => {
      return {
        addDrawer,
        drawerOrder,
        drawers,
        removeDrawer
      };
    },
    [
      addDrawer,
      drawerOrder,
      drawers,
      removeDrawer
    ]
  );

  return (
    <DrawerContext.Provider
      value={value}
    >
      {children}
    </DrawerContext.Provider>
  );
};

DrawerProvider.displayName = 'DrawerProvider';

export default DrawerProvider;

export type DrawerProviderProps = PropsWithChildren<object>;
