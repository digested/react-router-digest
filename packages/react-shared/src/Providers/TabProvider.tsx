import {
  TabContext,
  tabDefaults,
  TabIndexContext
} from '../Contexts';
import {
  type ReactDigestTabProps
} from '../Types';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
  useState
} from 'react';

const TabProvider: FunctionComponent<TabProviderProps> = ({
  children
}) => {
  const {
    selectTab,
    tabIndex
  } = useContext(TabIndexContext);

  const [
    tabs,
    setTabs
  ] = useState(tabDefaults.tabs);

  const [
    tabOrder,
    setTabOrder
  ] = useState(tabDefaults.tabOrder);

  const addTab = useCallback(
    (
      tab: ReactDigestTabProps,
      goto?: boolean,
      position?: number
    ) => {
      const {
        tabId
      } = tab;

      setTabs(
        (previousTabs) => {
          return {
            ...previousTabs,
            [tabId]: tab
          };
        }
      );

      const index = Number.isSafeInteger(position) && Number(position);

      let goToIndex: null | number = null;

      setTabOrder(
        (previousTabOrder) => {
          if (
            index &&
            index > 0
          ) {
            const newTabOrder = [
              ...previousTabOrder
            ].splice(
              index,
              0,
              tabId
            );

            if (goto) {
              goToIndex = index;
            }

            return newTabOrder;
          } else {
            const newTabOrder = [
              ...previousTabOrder,
              tabId
            ];

            if (goto) {
              goToIndex = newTabOrder.length - 1;
            }

            return newTabOrder;
          }
        }
      );

      if (goToIndex) {
        selectTab(goToIndex);
      }
    },
    [
      selectTab
    ]
  );

  const updateTab = useCallback(
    (
      {
        tabId,
        ...tabProperties
      }: ReactDigestTabProps
    ) => {
      setTabs(
        (previousTabs) => {
          if (
            previousTabs[tabId]
          ) {
            const updatedTabs = {
              ...previousTabs,
              [tabId]: {
                ...previousTabs[tabId],
                ...tabProperties
              }
            };

            return updatedTabs;
          } else {
            return previousTabs;
          }
        }
      );
    },
    []
  );

  const removeTab = useCallback(
    (
      tabId: string
    ) => {
      let newTabOrderLength: null | number = null;

      setTabOrder(
        (previousTabOrder) => {
          const newTabOrder = previousTabOrder.filter(
            (id) => {
              return id !== tabId;
            }
          );

          newTabOrderLength = newTabOrder.length;

          return newTabOrder;
        }
      );

      if (newTabOrderLength !== null) {
        selectTab(
          (currentTabIndex) => {
            if (
              newTabOrderLength === currentTabIndex
            ) {
              return currentTabIndex - 1;
            } else {
              return currentTabIndex;
            }
          }
        );

        setTabs(
          (previousTabs) => {
            if (previousTabs[tabId]) {
              const {
                // eslint-disable-next-line @typescript-eslint/no-unused-vars
                [tabId]: omit,
                ...filteredTabs
              } = previousTabs;

              return filteredTabs;
            } else {
              return previousTabs;
            }
          }
        );
      }
    },
    [
      selectTab
    ]
  );

  const selectedTabId = useMemo(
    () => {
      const tabId = tabs[tabOrder[tabIndex]]?.tabId;

      return tabId;
    },
    [
      tabIndex,
      tabOrder,
      tabs
    ]
  );

  const value = useMemo(
    () => {
      return {
        addTab,
        removeTab,
        selectedTabId,
        tabOrder,
        tabs,
        updateTab
      };
    },
    [
      addTab,
      removeTab,
      selectedTabId,
      tabOrder,
      tabs,
      updateTab
    ]
  );

  return (
    <TabContext.Provider
      value={value}
    >
      {children}
    </TabContext.Provider>
  );
};

TabProvider.displayName = 'TabProvider';

export default TabProvider;

export type TabProviderProps = PropsWithChildren<object>;
