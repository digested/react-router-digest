import {
  type TabIndexContextProps
} from '../Types';
import {
  createContext
} from 'react';

const tabIndexDefaults: TabIndexContextProps = {
  selectTab: () => {},
  tabIndex: 0
};

const TabIndexContext = createContext<TabIndexContextProps>(tabIndexDefaults);

TabIndexContext.displayName = 'TabIndexContext';

export {
  TabIndexContext,
  tabIndexDefaults
};
