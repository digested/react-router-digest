import {
  type DrawerToggleContextProps
} from '../Types';
import {
  createContext
} from 'react';

const drawerToggleDefaults: DrawerToggleContextProps = {
  drawerOpen: false,
  toggleDrawer: () => {}
};

const DrawerToggleContext = createContext<DrawerToggleContextProps>(drawerToggleDefaults);

DrawerToggleContext.displayName = 'DrawerToggleContext';

export {
  DrawerToggleContext,
  drawerToggleDefaults
};
