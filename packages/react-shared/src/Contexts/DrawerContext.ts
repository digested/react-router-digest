import {
  type DrawerContextProps
} from '../Types';
import {
  createContext
} from 'react';

const drawerDefaults: DrawerContextProps = {
  addDrawer: () => {},
  drawerOrder: [],
  drawers: {},
  removeDrawer: () => {}
};

const DrawerContext = createContext<DrawerContextProps>(drawerDefaults);

DrawerContext.displayName = 'DrawerContext';

export {
  DrawerContext,
  drawerDefaults
};
