import {
  type DrawerIndexContextProps
} from '../Types';
import {
  createContext
} from 'react';

const drawerIndexDefaults: DrawerIndexContextProps = {
  drawerIndex: 0,
  selectDrawer: () => {}
};

const DrawerIndexContext = createContext<DrawerIndexContextProps>(drawerIndexDefaults);

DrawerIndexContext.displayName = 'DrawerIndexContext';

export {
  DrawerIndexContext,
  drawerIndexDefaults
};
