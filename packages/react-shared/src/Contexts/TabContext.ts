import {
  type TabContextProps
} from '../Types';
import {
  createContext
} from 'react';

const tabDefaults: TabContextProps = {
  addTab: () => {},
  removeTab: () => {},
  selectedTabId: '',
  tabOrder: [],
  tabs: {},
  updateTab: () => {}
};

const TabContext = createContext<TabContextProps>(tabDefaults);

TabContext.displayName = 'TabContext';

export {
  TabContext,
  tabDefaults
};
