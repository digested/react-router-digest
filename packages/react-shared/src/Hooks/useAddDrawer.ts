import {
  DrawerContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useAddDrawer = () => {
  const {
    addDrawer
  } = useContext(DrawerContext);

  return addDrawer;
};

export {
  useAddDrawer
};
