import {
  DrawerContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useRemoveDrawer = () => {
  const {
    removeDrawer
  } = useContext(DrawerContext);

  return removeDrawer;
};

export {
  useRemoveDrawer
};
