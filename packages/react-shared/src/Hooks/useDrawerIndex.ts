import {
  DrawerIndexContext
} from '../Contexts';
import {
  useContext
} from 'react';

// eslint-disable-next-line func-style
function useDrawerIndex () {
  const {
    drawerIndex
  } = useContext(DrawerIndexContext);

  return drawerIndex;
}

export {
  useDrawerIndex
};
