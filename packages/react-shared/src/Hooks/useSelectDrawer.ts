import {
  DrawerIndexContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useSelectDrawer = () => {
  const {
    selectDrawer
  } = useContext(DrawerIndexContext);

  return selectDrawer;
};

export {
  useSelectDrawer
};
