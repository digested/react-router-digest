import {
  DrawerContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDrawerOrder = () => {
  const {
    drawerOrder
  } = useContext(DrawerContext);

  return drawerOrder;
};

export {
  useDrawerOrder
};
