import {
  TabContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useTabs = () => {
  const {
    tabs
  } = useContext(TabContext);

  return tabs;
};

export {
  useTabs
};
