import {
  DrawerContext
} from '../Contexts';
import {
  useContext
} from 'react';

// eslint-disable-next-line func-style
function useDrawers () {
  const {
    drawers
  } = useContext(DrawerContext);

  return drawers;
}

export {
  useDrawers
};
