import {
  useTabIndex
} from './useTabIndex';
import {
  useTabOrder
} from './useTabOrder';

const useSelectedTabID = () => {
  const tabOrder = useTabOrder();
  const tabIndex = useTabIndex();

  const selectedTabID = tabOrder[tabIndex];

  return selectedTabID;
};

export {
  useSelectedTabID
};
