import {
  TabContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useRemoveTab = () => {
  const {
    removeTab
  } = useContext(TabContext);

  return removeTab;
};

export {
  useRemoveTab
};
