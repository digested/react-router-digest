import {
  TabContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useTabOrder = () => {
  const {
    tabOrder
  } = useContext(TabContext);

  return tabOrder;
};

export {
  useTabOrder
};
