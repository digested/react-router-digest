import {
  TabIndexContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useTabIndex = () => {
  const {
    tabIndex
  } = useContext(TabIndexContext);

  return tabIndex;
};

export {
  useTabIndex
};
