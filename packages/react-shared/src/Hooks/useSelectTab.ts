import {
  TabIndexContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useSelectTab = () => {
  const {
    selectTab
  } = useContext(TabIndexContext);

  return selectTab;
};

export {
  useSelectTab
};
