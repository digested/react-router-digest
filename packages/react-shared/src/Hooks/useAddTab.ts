import {
  TabContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useAddTab = () => {
  const {
    addTab
  } = useContext(TabContext);

  return addTab;
};

export {
  useAddTab
};
