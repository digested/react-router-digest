import {
  DrawerToggleContext
} from '../Contexts';
import {
  useContext
} from 'react';

// eslint-disable-next-line func-style
function useDrawerOpen () {
  const {
    drawerOpen
  } = useContext(DrawerToggleContext);

  return drawerOpen;
}

export {
  useDrawerOpen
};
