import {
  DrawerToggleContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDrawerStatus = () => {
  const {
    drawerOpen
  } = useContext(DrawerToggleContext);

  return drawerOpen;
};

export {
  useDrawerStatus
};
