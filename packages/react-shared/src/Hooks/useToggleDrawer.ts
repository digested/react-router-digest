import {
  DrawerToggleContext
} from '../Contexts';
import {
  useContext
} from 'react';

const useDrawerToggle = () => {
  const {
    toggleDrawer
  } = useContext(DrawerToggleContext);

  return toggleDrawer;
};

export {
  useDrawerToggle
};
