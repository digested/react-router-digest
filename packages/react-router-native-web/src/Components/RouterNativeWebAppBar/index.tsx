import {
  ReactNativeWebDigestAppBar
} from '@dgui/react-native-web';

ReactNativeWebDigestAppBar.displayName = 'RouterNativeWebAppBar';

export {
  ReactNativeWebDigestAppBar as default
} from '@dgui/react-native-web';
