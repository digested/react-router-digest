import {
  type ReactRouterNativeWebDigestTabProps
} from '../../Types';
import {
  ReactNativeWebDigestTab
} from '@dgui/react-native-web';
import {
  BackControl,
  RouteControl,
  TabRoute
} from '@dgui/react-router-shared';
import {
  type FunctionComponent
} from 'react';

const RouterNativeWebTab: FunctionComponent<ReactRouterNativeWebDigestTabProps> = ({
  children,
  ...props
}) => {
  return (
    <ReactNativeWebDigestTab
      {...props}
    >
      <TabRoute
        tabPath={props.tabPath}
      >
        <BackControl
          tabId={props.tabId}
        />
        <RouteControl
          tabId={props.tabId}
        />
        {children}
      </TabRoute>
    </ReactNativeWebDigestTab>
  );
};

RouterNativeWebTab.displayName = 'RouterNativeWebTab';

export default RouterNativeWebTab;
