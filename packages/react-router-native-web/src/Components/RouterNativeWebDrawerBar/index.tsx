import {
  ReactNativeWebDigestDrawerBar
} from '@dgui/react-native-web';

ReactNativeWebDigestDrawerBar.displayName = 'RouterNativeWebDrawerBar';

export {
  ReactNativeWebDigestDrawerBar as default
} from '@dgui/react-native-web';
