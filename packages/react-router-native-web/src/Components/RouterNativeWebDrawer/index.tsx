import {
  ReactNativeWebDigestDrawer
} from '@dgui/react-native-web';

ReactNativeWebDigestDrawer.displayName = 'RouterNativeWebDrawer';

export {
  ReactNativeWebDigestDrawer as default
} from '@dgui/react-native-web';
