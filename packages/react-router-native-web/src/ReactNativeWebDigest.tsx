import {
  type ReactNativeWebDigestProps,
  ReactNativeWebDigest as VanillaReactNativeWebDigest
} from '@dgui/react-native-web';
import {
  type FunctionComponent,
  useCallback,
  useRef
} from 'react';

const ReactNativeWebDigest: FunctionComponent<ReactNativeWebDigestProps> = ({
  children,
  onSelectTab: givenSelectTab,
  tabIndex: givenTabIndex,
  ...rest
}) => {
  const callback = useRef<((tabIndex: number) => void) | undefined>(undefined);

  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      if (callback.current) {
        callback.current(tabIndex);
      }

      if (givenSelectTab) {
        givenSelectTab(tabIndex);
      }
    },
    [
      givenSelectTab
    ]
  );

  return (
    <VanillaReactNativeWebDigest
      onSelectTab={handleSelectTab}
      tabIndex={givenTabIndex}
      {...rest}
    >
      {children}
    </VanillaReactNativeWebDigest>
  );
};

ReactNativeWebDigest.displayName = 'ReactNativeWebDigest';

export default ReactNativeWebDigest;
