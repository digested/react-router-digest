import {
  type ReactRouterNativeWebDigestDrawer,
  type ReactRouterNativeWebDigestTab
} from '.';
import {
  type ReactNativeWebDigestAppBarProps,
  type ReactNativeWebDigestDrawerBarProps,
  type ReactNativeWebDigestDrawerProps,
  type ReactNativeWebDigestProps,
  type ReactNativeWebDigestTabProps
} from '@dgui/react-native-web';
import {
  type ReactRouterDigestAppBarProps,
  type ReactRouterDigestDrawerBarProps,
  type ReactRouterDigestDrawerProps,
  type ReactRouterDigestProps,
  type ReactRouterDigestTabProps
} from '@dgui/react-router-shared';
import {
  type ReactRouterWebProvidersProps
} from '@dgui/react-router-web-shared';
import {
  type ReactElement
} from 'react';

export type ReactRouterNativeWebDigestAppBarProps = ReactNativeWebDigestAppBarProps & ReactRouterDigestAppBarProps & {};

export type ReactRouterNativeWebDigestDrawerBarProps = ReactNativeWebDigestDrawerBarProps & ReactRouterDigestDrawerBarProps & {};

export type ReactRouterNativeWebDigestDrawerProps = ReactNativeWebDigestDrawerProps & ReactRouterDigestDrawerProps & {};

export type ReactRouterNativeWebDigestProps = Pick<ReactRouterWebProvidersProps, 'basename'> &
ReactNativeWebDigestProps &
ReactRouterDigestProps & {
  children: Array<ReactElement<typeof ReactRouterNativeWebDigestDrawer>> |
  Array<ReactElement<typeof ReactRouterNativeWebDigestTab>> |
  ReactElement<typeof ReactRouterNativeWebDigestDrawer> |
  ReactElement<typeof ReactRouterNativeWebDigestTab>;
};

export type ReactRouterNativeWebDigestTabProps = ReactNativeWebDigestTabProps & ReactRouterDigestTabProps & {
  tabPath: string;
};
