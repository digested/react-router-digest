import ReactWebDigest, {
} from './ReactWebDigest';
import {
  type ReactRouterWebDigestProps
} from './Types';
import {
  DigestRoute,
  E404
} from '@dgui/react-router-shared';
import {
  ReactRouterWebProviders
} from '@dgui/react-router-web-shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const ReactRouterWebDigest: FunctionComponent<ReactRouterWebDigestProps> = ({
  basename,
  children,
  router,
  tabIndex: givenTabIndex = 0,
  ...rest
}) => {
  const defaultTab = useMemo(
    () => {
      return givenTabIndex;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const error404 = useMemo(
    () => {
      return (
        <E404
          defaultTab={defaultTab}
        />
      );
    },
    [
      defaultTab
    ]
  );

  return (
    <ReactRouterWebProviders
      basename={basename}
      router={router}
    >
      <ReactWebDigest
        {...rest}
        tabIndex={givenTabIndex}
      >
        <DigestRoute>
          {children}
          {error404}
        </DigestRoute>
      </ReactWebDigest>
    </ReactRouterWebProviders>
  );
};

ReactRouterWebDigest.displayName = 'ReactRouterWebDigest';

export default ReactRouterWebDigest;
