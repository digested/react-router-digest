import {
  ReactWebDigestAppBar
} from '@dgui/react-web';

ReactWebDigestAppBar.displayName = 'RouterWebAppBar';

export {
  ReactWebDigestAppBar as default
} from '@dgui/react-web';
