import {
  ReactWebDigestDrawerBar
} from '@dgui/react-web';

ReactWebDigestDrawerBar.displayName = 'RouterWebDrawerBar';

export {
  ReactWebDigestDrawerBar as default
} from '@dgui/react-web';
