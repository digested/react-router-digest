import {
  type ReactRouterWebDigestTabProps
} from '../../Types';
import {
  BackControl,
  RouteControl,
  TabRoute
} from '@dgui/react-router-shared';
import {
  ReactWebDigestTab
} from '@dgui/react-web';
import {
  type FunctionComponent
} from 'react';

const RouterWebTab: FunctionComponent<ReactRouterWebDigestTabProps> = ({
  children,
  ...props
}) => {
  return (
    <ReactWebDigestTab
      {...props}
    >
      <TabRoute
        tabPath={props.tabPath}
      >
        <BackControl
          tabId={props.tabId}
        />
        <RouteControl
          tabId={props.tabId}
        />
        {children}
      </TabRoute>
    </ReactWebDigestTab>
  );
};

RouterWebTab.displayName = 'RouterWebTab';

export default RouterWebTab;
