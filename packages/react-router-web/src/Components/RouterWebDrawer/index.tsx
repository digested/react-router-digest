import {
  ReactWebDigestDrawer
} from '@dgui/react-web';

ReactWebDigestDrawer.displayName = 'RouterWebDrawer';

export {
  ReactWebDigestDrawer as default
} from '@dgui/react-web';
