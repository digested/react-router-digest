import {
  type ReactRouterWebDigestDrawer,
  type ReactRouterWebDigestTab
} from '.';
import {
  type ReactRouterDigestAppBarProps,
  type ReactRouterDigestDrawerBarProps,
  type ReactRouterDigestDrawerProps,
  type ReactRouterDigestProps,
  type ReactRouterDigestTabProps
} from '@dgui/react-router-shared';
import {
  type ReactRouterWebProvidersProps
} from '@dgui/react-router-web-shared';
import {
  type ReactWebDigestAppBarProps,
  type ReactWebDigestDrawerBarProps,
  type ReactWebDigestDrawerProps,
  type ReactWebDigestProps,
  type ReactWebDigestTabProps
} from '@dgui/react-web';
import {
  type ReactElement
} from 'react';

export type ReactRouterWebDigestAppBarProps = ReactRouterDigestAppBarProps & ReactWebDigestAppBarProps & {};

export type ReactRouterWebDigestDrawerBarProps = ReactRouterDigestDrawerBarProps & ReactWebDigestDrawerBarProps & {};

export type ReactRouterWebDigestDrawerProps = ReactRouterDigestDrawerProps & ReactWebDigestDrawerProps & {};

export type ReactRouterWebDigestProps = Pick<ReactRouterWebProvidersProps, 'basename'> &
ReactRouterDigestProps &
ReactWebDigestProps & {
  children: Array<ReactElement<typeof ReactRouterWebDigestDrawer>> |
  Array<ReactElement<typeof ReactRouterWebDigestTab>> |
  ReactElement<typeof ReactRouterWebDigestDrawer> |
  ReactElement<typeof ReactRouterWebDigestTab>;
};

export type ReactRouterWebDigestTabProps = ReactRouterDigestTabProps & ReactWebDigestTabProps & {
  tabPath: string;
};
