import {
  type ReactWebDigestProps,
  ReactWebDigest as VanillaReactWebDigest
} from '@dgui/react-web';
import {
  type FunctionComponent,
  useCallback,
  useRef
} from 'react';

const ReactWebDigest: FunctionComponent<ReactWebDigestProps> = ({
  children,
  onSelectTab: givenSelectTab,
  tabIndex: givenTabIndex = 0,
  ...rest
}) => {
  const callback = useRef<(((tabIndex: number) => void) | undefined)>(undefined);

  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      if (callback.current) {
        callback.current(tabIndex);
      }

      if (givenSelectTab) {
        givenSelectTab(tabIndex);
      }
    },
    [
      givenSelectTab
    ]
  );

  return (
    <VanillaReactWebDigest
      onSelectTab={handleSelectTab}
      tabIndex={givenTabIndex}
      {...rest}
    >
      {children}
    </VanillaReactWebDigest>
  );
};

ReactWebDigest.displayName = 'ReactWebDigest';

export default ReactWebDigest;
