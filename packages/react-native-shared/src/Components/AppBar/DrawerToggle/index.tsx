import NativeBaseButton from '../../../Generics/NativeBaseButton';
import {
  useDrawerToggle
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useCallback
} from 'react';

const DrawerToggle: FunctionComponent<DrawerToggleProps> = () => {
  const drawerToggle = useDrawerToggle();

  const handlePress = useCallback(
    () => {
      drawerToggle();
    },
    [
      drawerToggle
    ]
  );

  return (
    <NativeBaseButton
      onPress={handlePress}
      title='Drawer Toggle'
    />
  );
};

export default memo(
  DrawerToggle
);

export type DrawerToggleProps = object;
