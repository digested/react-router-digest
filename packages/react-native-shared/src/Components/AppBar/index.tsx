import NativeBaseContainer from '../../Generics/NativeBaseContainer';
import {
  NativeBaseTabBar,
  NativeBaseTabBarTab
} from '../../Generics/NativeBaseTabBar';
import DrawerToggle from './DrawerToggle';
import {
  useSelectTab,
  useTabOrder
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';
import {
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: '#CCC',
    flexDirection: 'row'
  }
});

const AppBar: FunctionComponent<AppBarProps> = () => {
  const selectTab = useSelectTab();

  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      selectTab(tabIndex);
    },
    [
      selectTab
    ]
  );

  const tabOrder = useTabOrder();

  const tabs = useMemo(
    () => {
      return tabOrder.map(
        (
          tabId,
          index
        ) => {
          return (
            <NativeBaseTabBarTab
              key={tabId}
              onSelectTab={handleSelectTab}
              tabId={tabId}
              tabIndex={index}
            />
          );
        }
      );
    },
    [
      handleSelectTab,
      tabOrder
    ]
  );

  return (
    <NativeBaseContainer
      style={styles.tabBar}
    >
      <DrawerToggle />
      <NativeBaseTabBar>
        {tabs}
      </NativeBaseTabBar>
    </NativeBaseContainer>
  );
};

export default AppBar;

export type AppBarProps = object;
