import {
  NativeBaseTabBar as TabBar,
  NativeBaseTabBarTab as TabBarTab
} from '../../../Generics/NativeBaseTabBar';
import {
  useDrawerOrder,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const styles = StyleSheet.create({
  drawerBar: {
    backgroundColor: '#CCC',
    flexDirection: 'row'
  }
});

const DrawerBar: FunctionComponent<DrawerBarProps> = () => {
  const selectDrawer = useSelectDrawer();

  const handleSelectDrawer = useCallback(
    (
      drawerIndex: number
    ) => {
      selectDrawer(drawerIndex);
    },
    [
      selectDrawer
    ]
  );

  const drawerOrder = useDrawerOrder();

  const tabs = useMemo(
    () => {
      return drawerOrder.map(
        (
          tabId,
          index
        ) => {
          return (
            <TabBarTab
              key={tabId}
              onSelectTab={handleSelectDrawer}
              tabId={tabId}
              tabIndex={index}
            />
          );
        }
      );
    },
    [
      drawerOrder,
      handleSelectDrawer
    ]
  );

  return (
    <View
      style={styles.drawerBar}
    >
      <TabBar>
        {tabs}
      </TabBar>
    </View>
  );
};

DrawerBar.displayName = 'NativeWebDrawerBar';

export default DrawerBar;

export type DrawerBarProps = object;
