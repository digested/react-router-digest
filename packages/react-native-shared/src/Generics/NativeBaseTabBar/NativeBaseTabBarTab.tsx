import NativeButton from '../NativeBaseButton';
import {
  type FunctionComponent,
  useCallback
} from 'react';

const NativeBaseTabBarTab: FunctionComponent<NativeBaseTabBarTabProps> = ({
  onSelectTab,
  tabId,
  tabIndex,
  tabTitle
}) => {
  const handlePress = useCallback(
    () => {
      onSelectTab(
        tabIndex,
        tabId
      );
    },
    [
      onSelectTab,
      tabId,
      tabIndex
    ]
  );

  return (
    <NativeButton
      onPress={handlePress}
      title={tabTitle ?? tabId}
    />
  );
};

export default NativeBaseTabBarTab;

export type NativeBaseTabBarTabProps = {
  readonly onSelectTab: (
    tabIndex: number,
    tabId: string
  ) => void;
  readonly tabId: string;
  readonly tabIndex: number;
  readonly tabTitle?: string;
};
