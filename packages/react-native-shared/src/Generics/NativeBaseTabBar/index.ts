export {
  default as NativeBaseTabBar,
  type NativeBaseTabBarProps
} from './NativeBaseTabBar';
export {
  default as NativeBaseTabBarTab,
  type NativeBaseTabBarTabProps
} from './NativeBaseTabBarTab';
