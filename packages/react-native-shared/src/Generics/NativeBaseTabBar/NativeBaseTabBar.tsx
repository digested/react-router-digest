import NativeContainer from '../NativeBaseContainer';
import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  tabBar: {
    flex: 1,
    flexDirection: 'row'
  }
});

const NativeBaseTabBar: FunctionComponent<NativeBaseTabBarProps> = ({
  children
}) => {
  return (
    <NativeContainer
      style={styles.tabBar}
    >
      {children}
    </NativeContainer>
  );
};

export default NativeBaseTabBar;

export type NativeBaseTabBarProps = PropsWithChildren<object>;
