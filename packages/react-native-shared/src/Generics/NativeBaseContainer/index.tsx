import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  View,
  type ViewProps,
  type ViewStyle
} from 'react-native';

const NativeBaseContainer: FunctionComponent<NativeBaseContainerProps> = ({
  children,
  style,
  ...props
}) => {
  return (
    <View
      {...props}
      style={style}
    >
      {children}
    </View>
  );
};

NativeBaseContainer.displayName = 'NativeBaseContainer';

export default NativeBaseContainer;

export type NativeBaseContainerProps = Omit<ViewProps, 'style'> & PropsWithChildren<{
  readonly style: ViewStyle;
}>;
