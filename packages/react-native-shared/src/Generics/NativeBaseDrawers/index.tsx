import debounce from 'lodash.debounce';
import {
  type FunctionComponent,
  type PropsWithChildren,
  type ReactNode,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';
import {
  Animated,
  Easing,
  StyleSheet,
  useWindowDimensions,
  View,
  type ViewStyle
} from 'react-native';

const fadeAnim = new Animated.Value(0);

const easing = Easing.bezier(
  0.4,
  0,
  0.1,
  1
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%'
  },
  content: {
    flex: 1,
    opacity: fadeAnim as unknown as number
  },
  drawer: {
    backgroundColor: '#FFF',
    // boxShadow: '0 0 10px 5px rgba(0, 0, 0, 0.1)',
    elevation: 12,
    height: '100%',
    position: 'absolute'
    // zIndex: 0
  }
});

const NativeBaseDrawers: FunctionComponent<NativeBaseDrawersProps> = ({
  animationTime = 150,
  children,
  drawerContent,
  drawerWidth: givenDrawerWidth = 400,
  opacity = 0.6,
  open = true,
  position = 'left',
  style: givenDrawerStyle
}) => {
  const [
    screenWidth,
    setScreenWidth
  ] = useState(useWindowDimensions().width);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const setWidthDebounced = useCallback(
    debounce(
      setScreenWidth,
      100
    ),
    []
  );

  setWidthDebounced(useWindowDimensions().width);

  const determineNewLeftOffset = useCallback(
    (drawerWidth2: number) => {
      return position === 'left' ?
        drawerWidth2 :
        -drawerWidth2;
    },
    [
      position
    ]
  );

  const [
    leftOffset,
    setLeftOffset
  ] = useState(new Animated.Value(0));

  const maxDrawerWidth = useMemo(
    () => {
      return screenWidth - 100;
    },
    [
      screenWidth
    ]
  );

  const drawerWidth = useMemo(
    () => {
      return givenDrawerWidth > maxDrawerWidth ?
        maxDrawerWidth :
        givenDrawerWidth;
    },
    [
      givenDrawerWidth,
      maxDrawerWidth
    ]
  );

  useEffect(
    () => {
      if (open) {
        setLeftOffset(
          new Animated.Value(
            determineNewLeftOffset(drawerWidth)
          )
        );
      } else {
        setLeftOffset(new Animated.Value(0));
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      determineNewLeftOffset,
      drawerWidth
    ]
  );

  const openDrawer = useCallback(
    () => {
      Animated.parallel([
        Animated.timing(
          leftOffset,
          {
            duration: animationTime,
            easing,
            toValue: determineNewLeftOffset(drawerWidth),
            useNativeDriver: true
          }
        ),
        Animated.timing(
          fadeAnim,
          {
            duration: animationTime,
            easing,
            toValue: opacity,
            useNativeDriver: true
          }
        )
      ]).start();
    },
    [
      animationTime,
      determineNewLeftOffset,
      drawerWidth,
      leftOffset,
      opacity
    ]
  );

  const closeDrawer = useCallback(
    () => {
      Animated.parallel([
        Animated.timing(
          leftOffset,
          {
            duration: animationTime,
            easing,
            toValue: 0,
            useNativeDriver: true
          }
        ),
        Animated.timing(
          fadeAnim,
          {
            duration: animationTime,
            easing,
            toValue: 1,
            useNativeDriver: true
          }
        )
      ]).start();
    },
    [
      animationTime,
      leftOffset
    ]
  );

  useEffect(
    () => {
      if (open) {
        openDrawer();
      } else {
        closeDrawer();
      }
    },
    [
      closeDrawer,
      open,
      openDrawer
    ]
  );

  const drawerStyle = useMemo(
    () => {
      return [
        {
          [position]: -drawerWidth,
          transform: [
            {
              translateX: leftOffset
            }
          ],
          width: drawerWidth
        },
        styles.drawer,
        givenDrawerStyle
      ];
    },
    [
      drawerWidth,
      givenDrawerStyle,
      leftOffset,
      position
    ]
  );

  return (
    <View
      style={styles.container}
    >
      <Animated.View
        style={styles.content}
      >
        {children}
      </Animated.View>
      <Animated.View
        style={drawerStyle}
      >
        {drawerContent}
      </Animated.View>
    </View>
  );
};

NativeBaseDrawers.displayName = 'NativeBaseDrawers';

export default NativeBaseDrawers;

export type NativeBaseDrawersProps = PropsWithChildren<{
  readonly animationTime?: number;
  readonly drawerContent?: ReactNode;
  readonly drawerWidth?: number;
  readonly opacity?: number;
  readonly open?: boolean;
  readonly position?: 'left' | 'right';
  readonly style?: ViewStyle;
}>;
