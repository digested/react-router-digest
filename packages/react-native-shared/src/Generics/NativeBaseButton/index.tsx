import {
  type FunctionComponent
} from 'react';
import {
  Button,
  type ButtonProps
} from 'react-native';

const NativeBaseButton: FunctionComponent<NativeBaseButtonProps> = (
  props
) => {
  return (
    <Button
      {...props}
    />
  );
};

NativeBaseButton.displayName = 'NativeBaseButton';

export default NativeBaseButton;

export type NativeBaseButtonProps = ButtonProps;
