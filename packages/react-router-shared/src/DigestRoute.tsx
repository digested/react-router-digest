import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  Route,
  Routes
} from 'react-router';

const DigestRoute: FunctionComponent<DigestRouteProps> = ({
  children
}) => {
  return (
    <Routes>
      <Route
        element={children}
        path='*'
      />
    </Routes>
  );
};

DigestRoute.displayName = 'DigestRoute';

export default DigestRoute;

export type DigestRouteProps = PropsWithChildren<object>;
