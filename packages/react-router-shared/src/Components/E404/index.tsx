import {
  useTabOrder,
  useTabs
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useEffect,
  useRef
} from 'react';
import {
  generatePath,
  useLocation,
  useNavigate
} from 'react-router';

const E404: FunctionComponent<E404Props> = ({
  defaultTab
}) => {
  const hasRendered = useRef(false);

  const navigate = useNavigate();
  const location = useLocation();

  const tabOrder = useTabOrder();
  const tabs = useTabs();

  useEffect(
    () => {
      if (
        hasRendered.current &&
        !Object.values(tabs).some(
          (tab) => {
            const path = generatePath(
              tab?.tabPath || '',
              {
                tabId: tab?.tabId
              }
            );

            return location.pathname.includes(path);
          }
        )
      ) {
        const tab = tabs[tabOrder[defaultTab]] || tabs[tabOrder[0]];

        const generatedPath = generatePath(
          tab?.tabPath || '',
          {
            tabId: tab?.tabId
          }
        );

        navigate(
          generatedPath,
          {
            relative: 'route',
            replace: true
          }
        );
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      tabs,
      hasRendered.current
    ]
  );

  useEffect(
    () => {
      hasRendered.current = true;
    },
    []
  );

  return null;
};

E404.displayName = 'E404';

export default E404;

export type E404Props = {
  readonly defaultTab: number;
};
