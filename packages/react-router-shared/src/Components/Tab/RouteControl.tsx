import {
  useTabIndex,
  useTabOrder,
  useTabs
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo,
  useRef
} from 'react';
import {
  generatePath,
  useLocation,
  useNavigate,
  useParams
} from 'react-router';

const RouteControl: FunctionComponent<RouteControlProps> = ({
  tabId
}) => {
  const hasRendered = useRef(false);

  const {
    tabId: routeId
  } = useParams();

  const navigate = useNavigate();
  const location = useLocation();

  const tabIndex = useTabIndex();
  const tabOrder = useTabOrder();
  const tabs = useTabs();

  const {
    tabId: selectedTabId,
    tabPath: selectedTabPath
  } = useMemo(
    () => {
      return tabs[tabOrder[tabIndex]] || {};
    },
    [
      tabIndex,
      tabOrder,
      tabs
    ]
  );

  useEffect(
    () => {
      // Only current tab should be handling logic
      if (
        hasRendered.current &&
        routeId &&
        selectedTabId &&
        tabId === selectedTabId
      ) {
        const indexChange = tabIndex !== tabOrder.indexOf(routeId);

        if (indexChange) {
          const generatedPath = generatePath(
            selectedTabPath,
            {
              tabId: selectedTabId
            }
          );

          if (
            !location.pathname.includes(generatedPath)
          ) {
            navigate(
              '/' + generatedPath,
              {
                relative: 'path'
              }
            );
          }
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      tabIndex
    ]
  );

  useEffect(
    () => {
      hasRendered.current = true;
    },
    []
  );

  return null;
};

export default RouteControl;

export type RouteControlProps = {
  readonly tabId: string;
};
