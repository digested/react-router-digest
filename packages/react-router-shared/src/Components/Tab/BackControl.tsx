import {
  useSelectedTabID,
  useSelectTab,
  useTabOrder
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useEffect
} from 'react';
import {
  useParams
} from 'react-router';

const BackControl: FunctionComponent<BackControlProps> = ({
  tabId
}) => {
  const {
    tabId: routeId
  } = useParams();

  const selectedTabID = useSelectedTabID();
  const tabOrder = useTabOrder();
  const selectTab = useSelectTab();

  useEffect(
    () => {
      // Only current tab should be handling logic
      if (
        routeId &&
        selectedTabID &&
        tabId === selectedTabID &&
        tabId !== routeId
      ) {
        const currentTabIndex = tabOrder.indexOf(tabId);
        const routeTabIndex = tabOrder.indexOf(routeId);

        if (
          routeTabIndex === -1
        ) {
          // Route does not exist
        } else if (
          currentTabIndex !== routeTabIndex
        ) {
          selectTab(routeTabIndex);
        }
      }
    },
    // DO NOT TOUCH - WILL CAUSE INFINITE LOOP
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      routeId
    ]
  );

  return null;
};

export default memo(BackControl);

export type BackControlProps = {
  readonly tabId: string;
};
