import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  Outlet,
  Route,
  Routes
} from 'react-router';

const TabRoute: FunctionComponent<TabRouteProps> = ({
  children,
  tabPath
}) => {
  return (
    <>
      <Routes>
        <Route
          element={children}
          path={tabPath}
        />
      </Routes>
      <Outlet />
    </>
  );
};

export default TabRoute;

export type TabRouteProps = PropsWithChildren<{
  readonly tabPath: string;
}>;
