export type ReactRouterDigestAppBarProps = object;
export type ReactRouterDigestDrawerBarProps = object;
export type ReactRouterDigestDrawerProps = object;
export type ReactRouterDigestProps = {
  router?: boolean;
};
export type ReactRouterDigestTabProps = object;
