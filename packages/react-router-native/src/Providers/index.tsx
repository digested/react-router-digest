import ReactRouterNativeProvider, {
  type ReactRouterNativeProviderProps
} from './ReactRouterNativeProvider';
import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ProvidersProps> = ({
  children,
  router,
  ...props
}) => {
  return (
    <ReactRouterNativeProvider
      router={router}
      {...props}
    >
      {children}
    </ReactRouterNativeProvider>
  );
};

Providers.displayName = 'ReactRouterNativeProviders';

export default Providers;

export type ProvidersProps = PropsWithChildren<{
  readonly router?: boolean;
}> & ReactRouterNativeProviderProps;
