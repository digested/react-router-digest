import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  NativeRouter,
  type NativeRouterProps
} from 'react-router-native';

const future = {
  v7_relativeSplatPath: true,
  v7_startTransition: false,
};

const ReactRouterNativeProvider: FunctionComponent<ReactRouterNativeProviderProps> = ({
  children,
  router,
  ...props
}) => {
  return router ?
    // eslint-disable-next-line @stylistic/no-extra-parens
    (
      <>
        {children}
      </>
    ) :
    // eslint-disable-next-line @stylistic/no-extra-parens
    (
      <NativeRouter
        future={future}
        {...props}
      >
        {children}
      </NativeRouter>
    );
};

ReactRouterNativeProvider.displayName = 'ReactRouterNativeProvider';

export default ReactRouterNativeProvider;

export type ReactRouterNativeProviderProps = NativeRouterProps & PropsWithChildren<{
  readonly router?: boolean;
}>;
