import RouterNativeBack from './Components/RouterNativeBack';
import Providers from './Providers';
import ReactNativeDigest from './ReactNativeDigest';
import {
  type ReactRouterNativeDigestProps
} from './Types';
import {
  DigestRoute,
  E404
} from '@dgui/react-router-shared';
import {
  type FunctionComponent,
  useMemo
} from 'react';

const ReactRouterNativeDigest: FunctionComponent<ReactRouterNativeDigestProps> = ({
  basename,
  children,
  router,
  tabIndex: givenTabIndex = 0,
  ...rest
}) => {
  const defaultTab = useMemo(
    () => {
      return givenTabIndex;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const error404 = useMemo(
    () => {
      return (
        <E404
          defaultTab={defaultTab}
        />
      );
    },
    [
      defaultTab
    ]
  );

  return (
    <Providers
      basename={basename}
      router={router}
    >
      <ReactNativeDigest
        {...rest}
        tabIndex={givenTabIndex}
      >
        <RouterNativeBack />
        <DigestRoute>
          {children}
          {error404}
        </DigestRoute>
      </ReactNativeDigest>
    </Providers>
  );
};

ReactRouterNativeDigest.displayName = 'ReactRouterNativeDigest';

export default ReactRouterNativeDigest;
