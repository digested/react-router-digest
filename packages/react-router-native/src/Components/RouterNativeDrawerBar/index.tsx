import {
  ReactNativeDigestDrawerBar
} from '@dgui/react-native';

ReactNativeDigestDrawerBar.displayName = 'RouterNativeDrawerBar';

export {
  ReactNativeDigestDrawerBar as default
} from '@dgui/react-native';
