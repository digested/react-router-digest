import {
  type FunctionComponent,
  useEffect
} from 'react';
import {
  BackHandler

} from 'react-native';
import {
  useNavigate
} from 'react-router-native';

const RouterNativeBack: FunctionComponent<object> = () => {
  const navigate = useNavigate();

  useEffect(
    () => {
      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        () => {
          navigate(-1);

          return true;
        }
      );

      return () => {
        backHandler.remove();
      };
    },
    [
      navigate
    ]
  );

  return null;
};

RouterNativeBack.displayName = 'RouterNativeBack';

export default RouterNativeBack;
