import {
  type ReactRouterNativeDigestTabProps
} from '../../Types';
import {
  ReactNativeDigestTab
} from '@dgui/react-native';
import {
  BackControl,
  RouteControl,
  TabRoute
} from '@dgui/react-router-shared';
import {
  type FunctionComponent
} from 'react';

const RouterNativeTab: FunctionComponent<ReactRouterNativeDigestTabProps> = ({
  children,
  ...props
}) => {
  return (
    <ReactNativeDigestTab
      {...props}
    >
      <TabRoute
        tabPath={props.tabPath}
      >
        <BackControl
          tabId={props.tabId}
        />
        <RouteControl
          tabId={props.tabId}
        />
        {children}
      </TabRoute>
    </ReactNativeDigestTab>
  );
};

RouterNativeTab.displayName = 'RouterNativeTab';

export default RouterNativeTab;
