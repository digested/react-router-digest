import {
  ReactNativeDigestAppBar
} from '@dgui/react-native';

ReactNativeDigestAppBar.displayName = 'RouterNativeAppBar';

export {
  ReactNativeDigestAppBar as default
} from '@dgui/react-native';
