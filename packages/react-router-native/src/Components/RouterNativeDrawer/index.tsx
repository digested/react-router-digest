import {
  ReactNativeDigestDrawer
} from '@dgui/react-native';

ReactNativeDigestDrawer.displayName = 'RouterNativeDrawer';

export {
  ReactNativeDigestDrawer as default
} from '@dgui/react-native';
