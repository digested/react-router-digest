import {
  type ReactNativeDigestProps,
  ReactNativeDigest as VanillaReactNativeDigest
} from '@dgui/react-native';
import {
  type FunctionComponent,
  useCallback,
  useRef
} from 'react';

const ReactNativeDigest: FunctionComponent<ReactNativeDigestProps> = ({
  children,
  onSelectTab: givenSelectTab,
  tabIndex: givenTabIndex = 0,
  ...rest
}) => {
  const callback = useRef<((tabIndex: number) => void) | undefined>(undefined);

  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      if (callback.current) {
        callback.current(tabIndex);
      }

      if (givenSelectTab) {
        givenSelectTab(tabIndex);
      }
    },
    [
      givenSelectTab
    ]
  );

  return (
    <VanillaReactNativeDigest
      onSelectTab={handleSelectTab}
      tabIndex={givenTabIndex}
      {...rest}
    >
      {children}
    </VanillaReactNativeDigest>
  );
};

ReactNativeDigest.displayName = 'ReactNativeDigest';

export default ReactNativeDigest;

