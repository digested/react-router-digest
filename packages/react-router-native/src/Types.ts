import {
  type ReactRouterNativeDigestDrawer,
  type ReactRouterNativeDigestTab
} from '.';
import {
  type ProvidersProps
} from './Providers';
import {
  type ReactNativeDigestAppBarProps,
  type ReactNativeDigestDrawerBarProps,
  type ReactNativeDigestDrawerProps,
  type ReactNativeDigestProps,
  type ReactNativeDigestTabProps
} from '@dgui/react-native';
import {
  type ReactRouterDigestAppBarProps,
  type ReactRouterDigestDrawerBarProps,
  type ReactRouterDigestDrawerProps,
  type ReactRouterDigestProps,
  type ReactRouterDigestTabProps
} from '@dgui/react-router-shared';
import {
  type ReactElement
} from 'react';

export type ReactRouterNativeDigestAppBarProps = ReactNativeDigestAppBarProps & ReactRouterDigestAppBarProps & {};

export type ReactRouterNativeDigestDrawerBarProps = ReactNativeDigestDrawerBarProps & ReactRouterDigestDrawerBarProps & {};

export type ReactRouterNativeDigestDrawerProps = ReactNativeDigestDrawerProps & ReactRouterDigestDrawerProps & {};

export type ReactRouterNativeDigestProps = Pick<ProvidersProps, 'basename'> &
ReactNativeDigestProps &
ReactRouterDigestProps & {
  children: Array<ReactElement<typeof ReactRouterNativeDigestDrawer>> |
  Array<ReactElement<typeof ReactRouterNativeDigestTab>> |
  ReactElement<typeof ReactRouterNativeDigestDrawer> |
  ReactElement<typeof ReactRouterNativeDigestTab>;
};

export type ReactRouterNativeDigestTabProps = ReactNativeDigestTabProps & ReactRouterDigestTabProps & {
  tabPath: string;
};
