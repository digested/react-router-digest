import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';
import {
  BrowserRouter,
  type BrowserRouterProps
} from 'react-router-dom';

const future = {
  v7_relativeSplatPath: true,
  v7_startTransition: true,
};

const ReactRouterBrowserProvider: FunctionComponent<ReactRouterBrowserProviderProps> = ({
  children,
  router,
  ...props
}) => {
  return router ?
    // eslint-disable-next-line @stylistic/no-extra-parens
    (
      <>
        {children}
      </>
    ) :
    // eslint-disable-next-line @stylistic/no-extra-parens
    (
      <BrowserRouter
        future={future}
        {...props}
      >
        {children}
      </BrowserRouter>
    );
};

ReactRouterBrowserProvider.displayName = 'ReactRouterBrowserProvider';

export default ReactRouterBrowserProvider;

export type ReactRouterBrowserProviderProps = BrowserRouterProps & PropsWithChildren<{
  readonly router?: boolean;
}>;
