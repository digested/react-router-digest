import ReactRouterBrowserProvider, {
  type ReactRouterBrowserProviderProps
} from './ReactRouterBrowserProvider';
import {
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const Providers: FunctionComponent<ReactRouterWebProvidersProps> = ({
  children,
  router,
  ...props
}) => {
  return (
    <ReactRouterBrowserProvider
      router={router}
      {...props}
    >
      {children}
    </ReactRouterBrowserProvider>
  );
};

Providers.displayName = 'ReactRouterWebProviders';

export default Providers;

export type ReactRouterWebProvidersProps = PropsWithChildren<object> & ReactRouterBrowserProviderProps;
