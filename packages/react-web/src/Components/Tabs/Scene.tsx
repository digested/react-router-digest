/*
  eslint-disable
  canonical/filename-match-exported
*/

import {
  WebGatewayDest as GatewayDestination
} from '../../Generics/WebGateway';
import {
  useTabs
} from '@dgui/react-shared';
import {
  WebBaseHorse
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  memo,
  useMemo
} from 'react';

const Scene: FunctionComponent<SceneProps> = ({
  tabId
}) => {
  const tabs = useTabs();

  const {
    swipe
  } = tabs[tabId];

  const gatewayId = useMemo(
    () => {
      return `tab-${tabId}`;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <WebBaseHorse
      swipe={swipe}
    >
      <GatewayDestination
        gatewayId={gatewayId}
      />
    </WebBaseHorse>
  );
};

const SceneSwiperSlide = memo(
  Scene
);

export type SceneProps = {
  readonly tabId: string;
};

SceneSwiperSlide.displayName = 'SceneSwiperSlide';

export default SceneSwiperSlide;
