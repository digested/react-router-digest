import {
  type ReactWebDigestDrawerBarProps
} from '../../Types';
import DrawerBar from './DrawerBar';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent
} from 'react';

const WebDrawerBar: FunctionComponent<ReactWebDigestDrawerBarProps> = ({
  children
}) => {
  return (
    <WebBaseGateway
      gatewayId='ReactWebDigestDrawerBar'
    >
      {children ?? <DrawerBar />}
    </WebBaseGateway>
  );
};

WebDrawerBar.displayName = 'WebDrawerBar';

export default WebDrawerBar;
