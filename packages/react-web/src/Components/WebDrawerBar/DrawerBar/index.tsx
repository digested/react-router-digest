import {
  WebTabBar as TabBar,
  WebTabBarTab as TabBarTab
} from '../../../Generics/WebTabBar';
import {
  useDrawerOrder,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  drawerBar: {
    backgroundColor: '#CCC',
    flexDirection: 'row'
  }
};

const DrawerBar: FunctionComponent<DrawerBarProps> = () => {
  const selectDrawer = useSelectDrawer();

  const handleSelectDrawer = useCallback(
    (
      drawerIndex: number
    ) => {
      selectDrawer(drawerIndex);
    },
    [
      selectDrawer
    ]
  );

  const drawerOrder = useDrawerOrder();

  const tabs = useMemo(
    () => {
      return drawerOrder.map(
        (
          tabId,
          index
        ) => {
          return (
            <TabBarTab
              key={tabId}
              onSelectTab={handleSelectDrawer}
              tabId={tabId}
              tabIndex={index}
            />
          );
        }
      );
    },
    [
      drawerOrder,
      handleSelectDrawer
    ]
  );

  return (
    <div
      style={styles.drawerBar}
    >
      <TabBar>
        {tabs}
      </TabBar>
    </div>
  );
};

DrawerBar.displayName = 'WebDrawerBar';

export default DrawerBar;

export type DrawerBarProps = object;
