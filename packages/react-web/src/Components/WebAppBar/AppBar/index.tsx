import WebBaseContainer from '../../../Generics/WebContainer';
import {
  WebTabBar,
  WebTabBarTab
} from '../../../Generics/WebTabBar';
import DrawerToggle from './DrawerToggle';
import {
  useSelectTab,
  useTabOrder
} from '@dgui/react-shared';
import {
  type CSSProperties,
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  tabBar: {
    backgroundColor: '#CCC',
    display: 'flex',
    flexDirection: 'row'
  }
};

const AppBar: FunctionComponent<AppBarProps> = () => {
  const selectTab = useSelectTab();

  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      selectTab(tabIndex);
    },
    [
      selectTab
    ]
  );

  const tabOrder = useTabOrder();

  const tabs = useMemo(
    () => {
      return tabOrder.map(
        (
          tabId,
          index
        ) => {
          return (
            <WebTabBarTab
              key={tabId}
              onSelectTab={handleSelectTab}
              tabId={tabId}
              tabIndex={index}
            />
          );
        }
      );
    },
    [
      handleSelectTab,
      tabOrder
    ]
  );

  return (
    <WebBaseContainer
      style={styles.tabBar}
    >
      <DrawerToggle />
      <WebTabBar>
        {tabs}
      </WebTabBar>
    </WebBaseContainer>
  );
};

export default AppBar;

export type AppBarProps = object;
