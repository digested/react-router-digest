import WebBaseButton from '../../../../Generics/WebButton';
import {
  useDrawerToggle
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useCallback
} from 'react';

const DrawerToggle: FunctionComponent<DrawerToggleProps> = () => {
  const toggleDrawer = useDrawerToggle();

  const handlePress = useCallback(
    () => {
      toggleDrawer();
    },
    [
      toggleDrawer
    ]
  );

  return (
    <WebBaseButton
      onClick={handlePress}
      title='Drawer Toggle'
    />
  );
};

export default memo(
  DrawerToggle
);

export type DrawerToggleProps = object;
