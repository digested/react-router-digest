import {
  type ReactWebDigestAppBarProps
} from '../../Types';
import AppBar from './AppBar';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent
} from 'react';

const WebAppBar: FunctionComponent<ReactWebDigestAppBarProps> = ({
  children
}) => {
  return (
    <WebBaseGateway
      gatewayId='ReactWebDigestAppBar'
    >
      {children ?? <AppBar />}
    </WebBaseGateway>
  );
};

WebAppBar.displayName = 'WebAppBar';

export default WebAppBar;
