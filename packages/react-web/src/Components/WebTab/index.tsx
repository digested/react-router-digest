import {
  type ReactWebDigestTabProps
} from '../../Types';
import {
  useAddTab,
  useRemoveTab
} from '@dgui/react-shared';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const WebTab: FunctionComponent<ReactWebDigestTabProps> = ({
  children,
  ...props
}) => {
  // Tab ID should never change
  // Proper lifecycle is to unmount the component and add new with new ID
  const {
    gatewayId,
    tabId
  } = useMemo(
    () => {
      return {
        gatewayId: `tab-${props.tabId}`,
        tabId: props.tabId
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const addTab = useAddTab();
  const removeTab = useRemoveTab();

  useEffect(
    () => {
      addTab(props);

      return () => {
        removeTab(
          tabId
        );
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <WebBaseGateway
      gatewayId={gatewayId}
    >
      {children}
    </WebBaseGateway>
  );
};

WebTab.displayName = 'WebTab';

export default WebTab;
