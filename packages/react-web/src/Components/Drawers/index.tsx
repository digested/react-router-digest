import SideDrawers from '../../Generics/WebDrawers';
import {
  WebGatewayDest as GatewayDestination
} from '../../Generics/WebGateway';
import {
  type ReactWebDigestDrawersProps
} from '../../Types';
import Tabs from './Tabs';
import {
  useDrawerOpen
} from '@dgui/react-shared';
import {
  type CSSProperties,
  type FunctionComponent,
  type PropsWithChildren,
  useMemo
} from 'react';

const Drawers: FunctionComponent<DrawersProps> = (
  props
) => {
  const drawerOpen = useDrawerOpen();

  const appBar = useMemo(
    () => {
      return (
        <>
          <GatewayDestination
            gatewayId='ReactWebDigestDrawerBar'
          />
          <Tabs />
        </>
      );
    },
    []
  );

  return (
    <SideDrawers
      drawerContent={appBar}
      open={drawerOpen}
      {...props}
    />
  );
};

Drawers.displayName = 'WebDrawers';

export default Drawers;

export type DrawersProps = Omit<PropsWithChildren<ReactWebDigestDrawersProps>, 'style'> & {
  readonly style: CSSProperties;
};
