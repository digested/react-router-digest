import {
  type ReactWebDigestDrawerProps
} from '../../Types';
import {
  useAddDrawer,
  useRemoveDrawer
} from '@dgui/react-shared';
import {
  WebBaseGateway
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  useEffect,
  useMemo
} from 'react';

const WebDrawer: FunctionComponent<ReactWebDigestDrawerProps> = ({
  children,
  ...props
}) => {
  // Tab ID should never change
  // Proper lifecycle is to unmount the component and add new with new ID
  const {
    drawerId,
    gatewayId
  } = useMemo(
    () => {
      return {
        drawerId: props.drawerId,
        gatewayId: props.tabId ?
          `drawer-${props.drawerId}-${props.tabId}` :
          `drawer-${props.drawerId}`
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const addDrawer = useAddDrawer();
  const removeDrawer = useRemoveDrawer();

  useEffect(
    () => {
      addDrawer(props);

      return () => {
        removeDrawer(
          drawerId
        );
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <WebBaseGateway
      gatewayId={gatewayId}
    >
      {children}
    </WebBaseGateway>
  );
};

WebDrawer.displayName = 'WebDrawer';

export default WebDrawer;
