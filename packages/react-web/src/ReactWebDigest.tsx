import Drawers from './Components/Drawers';
import Tabs from './Components/Tabs';
import {
  WebGatewayDest as GatewayDestination
} from './Generics/WebGateway';
import Providers from './Providers';
import {
  type ReactWebDigestProps
} from './Types';
import {
  type CSSProperties,
  type FunctionComponent
} from 'react';

const ReactWebDigest: FunctionComponent<ReactWebDigestProps> = ({
  children,
  drawerAnimationTime,
  drawerIndex,
  drawerOpacity,
  drawerOpen,
  drawerPosition,
  drawerStyle,
  drawerWidth,
  onDrawerToggle,
  onSelectDrawer,
  onSelectTab,
  swipe,
  tabIndex
}) => {
  return (
    <Providers
      drawerIndex={drawerIndex}
      drawerOpen={drawerOpen}
      onDrawerToggle={onDrawerToggle}
      onSelectDrawer={onSelectDrawer}
      onSelectTab={onSelectTab}
      tabIndex={tabIndex}
    >
      <GatewayDestination
        gatewayId='ReactWebDigestAppBar'
      />
      <Drawers
        animationTime={drawerAnimationTime}
        opacity={drawerOpacity}
        position={drawerPosition}
        style={drawerStyle as CSSProperties}
        width={drawerWidth}
      >
        <Tabs
          swipe={swipe}
        />
      </Drawers>
      {children}
    </Providers>
  );
};

ReactWebDigest.displayName = 'ReactWebDigest';

export default ReactWebDigest;
