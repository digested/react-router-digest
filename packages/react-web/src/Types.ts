import {
  type ReactDigestAppBarProps,
  type ReactDigestDrawerBarProps,
  type ReactDigestDrawerProps,
  type ReactDigestDrawersProps,
  type ReactDigestProps,
  type ReactDigestTabProps
} from '@dgui/react-shared';

export type ReactWebDigestAppBarProps = ReactDigestAppBarProps;

export type ReactWebDigestDrawerBarProps = ReactDigestDrawerBarProps;

export type ReactWebDigestDrawerProps = ReactDigestDrawerProps;

export type ReactWebDigestDrawersProps = ReactDigestDrawersProps;

export type ReactWebDigestProps = ReactDigestProps;

export type ReactWebDigestTabProps = ReactDigestTabProps;

