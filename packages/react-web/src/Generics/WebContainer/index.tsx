import {
  type FunctionComponent,
  type HTMLAttributes
} from 'react';

const WebContainer: FunctionComponent<WebContainerProps> = ({
  children,
  style,
  ...props
}) => {
  return (
    <div
      {...props}
      style={style}
    >
      {children}
    </div>
  );
};

WebContainer.displayName = 'WebContainer';

export default WebContainer;

export type WebContainerProps = HTMLAttributes<HTMLDivElement>;
