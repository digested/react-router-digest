/*
  eslint-disable
  unicorn/prevent-abbreviations
*/

import {
  type GatewayDestProps
} from '@dgui/react-shared';
import {
  WebBaseGatewayContext
} from '@dgui/react-web-shared';
import {
  type FunctionComponent,
  useContext,
  useEffect,
  useRef
} from 'react';

const WebGatewayDest: FunctionComponent<WebGatewayDestProps> = ({
  gatewayId
}) => {
  const ref = useRef<HTMLDivElement | null>(null);

  const gatewayRegistry = useContext(WebBaseGatewayContext);

  useEffect(
    () => {
      gatewayRegistry?.addRef(
        gatewayId,
        ref
      );

      return () => {
        gatewayRegistry?.removeRef(gatewayId);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <div
      ref={ref}
    />
  );
};

WebGatewayDest.displayName = 'WebGatewayDest';

export default WebGatewayDest;

export type WebGatewayDestProps = GatewayDestProps;
