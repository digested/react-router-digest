import {
  type ButtonHTMLAttributes,
  type FunctionComponent
} from 'react';

const WebButton: FunctionComponent<WebButtonProps> = (
  props
) => {
  return (
    <button
      type='button'
      {...props}
    >
      {props.title}
    </button>
  );
};

WebButton.displayName = 'WebButton';

export default WebButton;

export type WebButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;
