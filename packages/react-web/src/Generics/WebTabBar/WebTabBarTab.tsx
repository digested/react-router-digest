import WebButton from '../WebButton';
import {
  type FunctionComponent,
  memo,
  useCallback
} from 'react';

const WebTabBarTab: FunctionComponent<WebTabBarTabProps> = ({
  onSelectTab,
  tabId,
  tabIndex,
  tabTitle
}) => {
  const handlePress = useCallback(
    () => {
      onSelectTab(
        tabIndex,
        tabId
      );
    },
    [
      onSelectTab,
      tabId,
      tabIndex
    ]
  );

  return (
    <WebButton
      onClick={handlePress}
      title={tabTitle ?? tabId}
    />
  );
};

export default memo(
  WebTabBarTab
);

export type WebTabBarTabProps = {
  readonly onSelectTab: (
    tabIndex: number,
    tabId: string
  ) => void;
  readonly tabId: string;
  readonly tabIndex: number;
  readonly tabTitle?: string;
};
