export {
  default as WebTabBar,
  type WebTabBarProps
} from './WebTabBar';
export {
  default as WebTabBarTab,
  type WebTabBarTabProps
} from './WebTabBarTab';
