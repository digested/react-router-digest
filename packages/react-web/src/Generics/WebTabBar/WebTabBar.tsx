import WebContainer from '../WebContainer';
import {
  type CSSProperties,
  type FunctionComponent,
  type PropsWithChildren
} from 'react';

const styles: {
  [key: string]: CSSProperties;
} = {
  tabBar: {
    flex: 1,
    flexDirection: 'row'
  }
};

const WebTabBar: FunctionComponent<WebTabBarProps> = ({
  children
}) => {
  return (
    <WebContainer
      style={styles.tabBar}
    >
      {children}
    </WebContainer>
  );
};

export default WebTabBar;

export type WebTabBarProps = PropsWithChildren<object>;
