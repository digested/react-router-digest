import {
  type CSSProperties,
  type FunctionComponent,
  type PropsWithChildren,
  type ReactNode,
  useMemo
} from 'react';
import Drawer from 'react-modern-drawer';

const styles: {
  [key: string]: CSSProperties;
} = {
  drawer: {
    display: 'flex',
    flexDirection: 'column',
    position: 'absolute'
  },
  drawerWrap: {
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    position: 'relative'
  }
};

const WebDrawers: FunctionComponent<WebDrawersProps> = ({
  animationTime = 150,
  children,
  drawerContent,
  drawerWidth = 400,
  opacity = 0.6,
  open = true,
  position = 'left',
  style: givenDrawerStyle
}) => {
  const style = useMemo(
    () => {
      return {
        ...styles.drawer,
        ...givenDrawerStyle
      };
    },
    [
      givenDrawerStyle
    ]
  );

  return (
    <div
      style={styles.drawerWrap}
    >
      {children}
      <Drawer
        direction={position}
        duration={animationTime}
        enableOverlay={false}
        open={open}
        overlayOpacity={opacity}
        size={drawerWidth}
        style={style}
      >
        {drawerContent}
      </Drawer>
    </div>
  );
};

WebDrawers.displayName = 'WebDrawers';

export default WebDrawers;

export type WebDrawersProps = PropsWithChildren<{
  readonly animationTime?: number;
  readonly drawerContent?: ReactNode;
  readonly drawerWidth?: number;
  readonly opacity?: number;
  readonly open?: boolean;
  readonly position?: 'left' | 'right';
  readonly style?: CSSProperties;
}>;
