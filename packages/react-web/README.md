<div align="center">
  <h1>
    <a
      href="https://gitlab.com/digested/dgui/"
    >
      @dgui/react-web
    </a>
  </h1>
</div>

<div align="center">
  <a
    href="https://commitizen.github.io/cz-cli/"
  >
    <img
      alt="Commitizen Friendly"
      src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000"
    />
  </a>
  <a
    href="https://github.com/semantic-release/semantic-release"
  >
    <img
      alt="Semantic Release"
      src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000"
    />
  </a>
</div>

<div align="center">
  <a
    href="https://gitlab.com/digested/dgui/-/commits/main"
  >
    <img
      alt="Build Status"
      src="https://gitlab.com/digested/dgui/badges/main/pipeline.svg"
    />
  </a>
  <a
    href="https://digested.gitlab.io/dgui/coverage"
  >
    <img
      alt="Coverage Report"
      src="https://gitlab.com/digested/dgui/badges/master/coverage.svg"
    />
  </a>
  <a
    href="https://www.npmjs.org/package/@dgui/react-web"
  >
    <img
      alt="NPM Version"
      src="http://img.shields.io/npm/v/@dgui/react-web.svg?maxAge=86400"
    />
  </a>
  <a
    href="https://www.gnu.org/licenses/lgpl-3.0.html"
  >
    <img
      alt="License"
      src="https://img.shields.io/npm/l/@dgui/react-web.svg?maxAge=2592000"
    />
  </a>
  <a
    href="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fdgui.svg?type=shield"
  >
    <img
      alt="FOSSA Status"
      src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fdgui.svg?type=shield&maxAge=3600"
    />
  </a>
  <a
    href="https://github.com/gajus/canonical"
  >
    <img
      alt="Canonical Code Style"
      src="https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000"
    />
  </a>
</div>
<br />
<br />

> A **`@dgui`** project package

See the [project page](https://gitlab.com/digested/dgui/) for documentation or the
[issues](https://gitlab.com/digested/dgui/-/issues) associated with this package.


