import {
  ReactNativeMaterialSharedAppBar,
  type ReactNativeMaterialSharedAppBarProps
} from '@dgui/react-native-material-bars-shared';
import {
  ReactNativeWebDigestAppBar
} from '@dgui/react-native-web';
import {
  type FunctionComponent
} from 'react';

const AppBar: FunctionComponent<AppBarProps> = (
  props
) => {
  return (
    <ReactNativeWebDigestAppBar>
      <ReactNativeMaterialSharedAppBar
        {...props}
      />
    </ReactNativeWebDigestAppBar>
  );
};

export default AppBar;

export type AppBarProps = ReactNativeMaterialSharedAppBarProps;
