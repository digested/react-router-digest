import {
  ReactNativeMaterialSharedDrawerBar,
  type ReactNativeMaterialSharedDrawerBarProps
} from '@dgui/react-native-material-bars-shared';
import {
  ReactNativeWebDigestDrawerBar
} from '@dgui/react-native-web';
import {
  type FunctionComponent
} from 'react';

const DrawerBar: FunctionComponent<DrawerBarProps> = (
  props
) => {
  return (
    <ReactNativeWebDigestDrawerBar>
      <ReactNativeMaterialSharedDrawerBar
        {...props}
      />
    </ReactNativeWebDigestDrawerBar>
  );
};

export default DrawerBar;

export type DrawerBarProps = ReactNativeMaterialSharedDrawerBarProps;
