import {
  type JSXElementConstructor,
  type ReactElement
} from 'react';
import {
  TabBarItem as AnnoyingTabBarItem,
  type TabBarItemProps as AnnoyingTabBarItemProps
} from 'react-native-tab-view';

export type RenderTabBarItem = (

  props: // eslint-disable-next-line @typescript-eslint/no-explicit-any
    AnnoyingTabBarItemProps<any> &
    { key: string }
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
) => ReactElement<any, JSXElementConstructor<any> | string>;

const TabBarItem: RenderTabBarItem = ({
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  key,
  ...props
}) => {
  return (
    <AnnoyingTabBarItem
      {...props}
    />
  );
};

export default TabBarItem;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TabBarProps = AnnoyingTabBarItemProps<any> & {};
