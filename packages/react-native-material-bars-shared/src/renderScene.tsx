import {
  SceneMap
} from 'react-native-tab-view';

export default (
  routes: Array<{
    key: string;
    title: string;
  }>
) => {
  return SceneMap(
    // eslint-disable-next-line unicorn/no-array-reduce
    routes.reduce<{ [key: string]: () => null }>(
      (
        groutes,
        route
      ) => {
        groutes[route.key] = () => {
          return null;
        };

        return groutes;
      },
    {})
  );
};
