import Tabs from './Tabs';
import {
  useDrawerIndex,
  useDrawerOrder,
  useDrawers,
  useSelectDrawer
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';
import {
  StyleSheet,
  View
} from 'react-native';

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: '#CCC',
    // boxShadow: '0 0 10px 5px rgba(0, 0, 0, 0.1)',
    display: 'flex',
    elevation: 6,
    flexDirection: 'row'
  }
});

const DrawerBar: FunctionComponent<DrawerBarProps> = () => {
  const drawerIndex = useDrawerIndex();
  const selectDrawer = useSelectDrawer();

  const handleSelectDrawer = useCallback(
    (
      tabIndex: number
    ) => {
      selectDrawer(tabIndex);
    },
    [
      selectDrawer
    ]
  );

  const drawerOrder = useDrawerOrder();
  const drawers = useDrawers();

  const routes = useMemo(
    () => {
      return drawerOrder.map(
        (
          drawerId
        ) => {
          return (
            {
              key: drawerId,
              title: drawers[drawerId]?.title ?? drawerId
            }
          );
        }
      );
    },
    [
      drawerOrder,
      drawers
    ]
  );

  return (
    <View
      style={styles.tabBar}
    >
      <Tabs
        defaultIndex={drawerIndex}
        onSelectTab={handleSelectDrawer}
        routes={routes}
      />
    </View>
  );
};

export default DrawerBar;

export type DrawerBarProps = object;
