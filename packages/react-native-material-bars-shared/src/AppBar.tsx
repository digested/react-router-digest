import DrawerToggle from './DrawerToggle';
import Tabs, {
} from './Tabs';
import {
  useSelectTab,
  useTabIndex,
  useTabOrder,
  useTabs
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  type PropsWithChildren,
  useCallback,
  useMemo
} from 'react';
import {
  type StyleProp,
  StyleSheet,
  View,
  type ViewStyle
} from 'react-native';

const styles = StyleSheet.create({
  appBar: {
    alignItems: 'center',
    backgroundColor: '#CCC',
    // boxShadow: '0 0 10px 5px rgba(0, 0, 0, 0.1)',
    display: 'flex',
    elevation: 12,
    flexDirection: 'row'
  }
});

const AppBar: FunctionComponent<AppBarProps> = ({
  appBarStyle: givenAppBarStyle,
  children,
  drawerButtonIcon,
  tabBarStyle,
  ...props
}) => {
  const appBarStyle: StyleProp<ViewStyle> = useMemo(
    () => {
      return {
        ...styles.appBar,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        ...givenAppBarStyle
      };
    },
    [
      givenAppBarStyle
    ]
  );

  const selectTab = useSelectTab();
  const tabIndex = useTabIndex();

  const handleSelectTab = useCallback(
    (
      index: number
    ) => {
      selectTab(index);
    },
    [
      selectTab
    ]
  );

  const tabOrder = useTabOrder();
  const tabs = useTabs();

  const routes = useMemo(
    () => {
      return tabOrder.map(
        (
          tabId
        ) => {
          return (
            {
              accessibilityLabel: tabs[tabId]?.title ?? tabId,
              icon: undefined,
              key: tabId,
              title: tabs[tabId]?.title ?? tabId
            }
          );
        }
      );
    },
    [
      tabOrder,
      tabs
    ]
  );

  return (
    <View
      style={appBarStyle}
    >
      <DrawerToggle
        icon={drawerButtonIcon}
      />
      <Tabs
        {...props}
        defaultIndex={tabIndex}
        onSelectTab={handleSelectTab}
        routes={routes}
        style={tabBarStyle}
      />
      {children}
    </View>
  );
};

export default AppBar;

export type AppBarProps = PropsWithChildren<{
  readonly appBarStyle?: StyleProp<ViewStyle>;
  readonly drawerButtonIcon?: FunctionComponent;
  readonly tabBarStyle?: StyleProp<ViewStyle>;
}>;
