import {
  useDrawerToggle
} from '@dgui/react-shared';
import {
  type FunctionComponent,
  memo,
  useCallback
} from 'react';
import {
  StyleSheet
} from 'react-native';
import {
  IconButton
} from 'react-native-paper';

const styles = StyleSheet.create({
  iconButton: {
    margin: 0
  }
});

const DrawerToggle: FunctionComponent<DrawerToggleProps> = ({
  icon
}) => {
  const toggleDrawer = useDrawerToggle();

  const handlePress = useCallback(
    () => {
      if (toggleDrawer) {
        toggleDrawer();
      }
    },
    [
      toggleDrawer
    ]
  );

  return (
    <IconButton
      accessibilityLabel='open drawer'
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-expect-error
      icon={icon ?? 'menu'}
      onPress={handlePress}
      style={styles.iconButton}
    />
  );
};

export default memo(
  DrawerToggle
);

export type DrawerToggleProps = {
  readonly icon?: FunctionComponent;
};
