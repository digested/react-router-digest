import renderScene from './renderScene';
import TabBar, {
  type RenderTabBar
} from './TabBar';
import TabBarItem, {
  type RenderTabBarItem
} from './TabBarItem';
import {
  type FunctionComponent,
  useCallback,
  useMemo
} from 'react';
import {
  type TabBarProps as AnnoyingTabBarProps,
  TabView as ReactNativeTabView
} from 'react-native-tab-view';

const Tabs: FunctionComponent<TabsProps> = ({
  defaultIndex,
  indicatorStyle,
  // labelStyle,
  onSelectTab,
  renderTabBarItem: givenRenderTabBarItem,
  routes = [],
  style,
  tabStyle
}) => {
  const handleSelectTab = useCallback(
    (
      tabIndex: number
    ) => {
      if (onSelectTab) {
        onSelectTab(tabIndex);
      }
    },
    [
      onSelectTab
    ]
  );

  const navigationState = useMemo(
    () => {
      // errors if default index is greater than number of routes
      const index = defaultIndex < routes.length ?
        defaultIndex :
        routes.length - 1;

      return {
        index,
        routes
      };
    },
    [
      defaultIndex,
      routes
    ]
  );

  const renderedScene = useMemo(
    () => {
      return renderScene(routes);
    },
    [
      routes
    ]
  );

  const renderTabBarItem = useMemo(
    () => {
      const render: RenderTabBarItem = (props) => {
        return givenRenderTabBarItem ?
          givenRenderTabBarItem(props) :
          TabBarItem(props);
      };

      return render;
    },
    [
      givenRenderTabBarItem
    ]
  );

  const renderTabBar = useMemo(
    () => {
      const render: RenderTabBar = (props) => {
        return TabBar({
          indicatorStyle,
          // labelStyle,
          renderTabBarItem,
          style,
          tabStyle,
          ...props
        });
      };

      return render;
    },
    [
      indicatorStyle,
      // labelStyle,
      renderTabBarItem,
      style,
      tabStyle
    ]
  );

  return (
    <ReactNativeTabView
      navigationState={navigationState}
      onIndexChange={handleSelectTab}
      renderScene={renderedScene}
      renderTabBar={renderTabBar}
    />
  );
};

export default Tabs;

export type TabsProps = Pick<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  AnnoyingTabBarProps<any>,
  'indicatorStyle' | 'style' | 'tabStyle'
> & {
  readonly defaultIndex: number;
  readonly onSelectTab?: (
    tabIndex: number
  ) => void;
  readonly renderTabBarItem?: RenderTabBarItem;
  readonly routes?: Array<{
    key: string;
    title: string;
  }>;
};
