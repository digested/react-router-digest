import {
  type ReactNode,
  useMemo
} from 'react';
import {
  type StyleProp,
  StyleSheet,
  type ViewStyle
} from 'react-native';
import {
  TabBar as AnnoyingTabBar,
  type TabBarProps as AnnoyingTabBarProps,
  type NavigationState,
  type SceneRendererProps
} from 'react-native-tab-view';

const styles = StyleSheet.create({
  indicator: {
    backgroundColor: '#1976d2'
  },
  label: {
    color: '#1976d2',
    textTransform: 'none'
  },
  tabBar: {
    backgroundColor: 'rgb(204, 204, 204)'
  },
  tabBarTab: {
    width: 'auto'
  }
});

export type RenderTabBar = (
  props: SceneRendererProps & {
    navigationState: NavigationState<{
      key: string;
      title: string;
    }>;
  }
) => ReactNode;

const TabBar = ({
  indicatorStyle: parentIndicatorStyle,
  // labelStyle: parentLabelStyle,
  style: parentStyle,
  tabStyle: parentTabStyle,
  ...props
}: TabBarProps) => {
  const style: StyleProp<ViewStyle> = useMemo(
    () => {
      return {
        ...styles.tabBar,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        ...parentStyle
      };
    },
    [
      parentStyle
    ]
  );

  const tabStyle: StyleProp<ViewStyle> = useMemo(
    () => {
      return {
        ...styles.tabBarTab,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        ...parentTabStyle
      };
    },
    [
      parentTabStyle
    ]
  );

  const indicatorStyle: StyleProp<ViewStyle> = useMemo(
    () => {
      return {
        ...styles.indicator,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        ...parentIndicatorStyle
      };
    },
    [
      parentIndicatorStyle
    ]
  );

  /*
  const labelStyle: StyleProp<ViewStyle> = useMemo(
    () => {
      return {
        ...styles.label,
        ...parentLabelStyle
      };
    },
    [
      parentLabelStyle
    ]
  );
  */

  return (
    <AnnoyingTabBar
      // activeColor='#1976d2'
      // inactiveColor='#1976d2'
      bounces
      indicatorStyle={indicatorStyle}
      // labelStyle={labelStyle}
      pressColor='#999'
      scrollEnabled
      style={style}
      tabStyle={tabStyle}
      {...props}
    />
  );
};

export default TabBar;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TabBarProps = AnnoyingTabBarProps<any> & {};
