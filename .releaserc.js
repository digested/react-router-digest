module.exports = {
  branches: [
    '+([0-9])?(.{+([0-9]),x}).x',
    {
      name: 'main',
      channel: undefined
    },
    {
      name: 'next',
      prerelease: true,
      channel: 'next'
    },
    {
      name: 'beta',
      prerelease: true,
      channel: 'beta'
    },
    {
      name: 'alpha',
      prerelease: true,
      channel: 'alpha'
    }
  ],
  ci: true,
  plugins: [
    "@semantic-release/commit-analyzer",
    [
      'semantic-release-lerna',
      {
        generateNotes: false,
        latch: 'prerelease',
        npmPublish: true,
        rootVersion: false
      }
    ],
    [
      '@semantic-release/git',
      {
        assets: [
          'lerna.json',
          'package.json',
          'demo/*/package.json',
          'packages/*/package.json'
        ]
      }
    ]
  ]
}
